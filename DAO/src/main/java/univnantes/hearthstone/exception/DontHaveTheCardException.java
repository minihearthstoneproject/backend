package univnantes.hearthstone.exception;

import java.util.logging.Logger;

import javax.validation.constraints.NotNull;

public class DontHaveTheCardException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	public static Logger logger = Logger.getLogger("DontHaveTheCardException");

	public DontHaveTheCardException(@NotNull String name) {
		logger.warning("la carte " + name + " n'est pas dans voytre main");
	}

	public DontHaveTheCardException() {
		logger.warning("la carte  n'est pas dans voytre main");
	}
}
