package univnantes.hearthstone.exception;

import java.util.logging.Logger;

import javax.validation.constraints.NotNull;


public class UnknowCardException extends Exception {

	private static final long serialVersionUID = -1071057875905556953L;
	public static Logger logger = Logger.getLogger("UnknowCardException");
	
	public UnknowCardException(@NotNull String name) {
		logger.warning("la carte "+name+" est inconnue");
	}
	
	public UnknowCardException() {
		logger.warning("la carte  est inconnue");
	}

}
