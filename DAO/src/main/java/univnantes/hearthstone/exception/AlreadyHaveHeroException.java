package univnantes.hearthstone.exception;

import java.util.logging.Logger;

public class AlreadyHaveHeroException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8773926715629306863L;
	/**
		 * 
		 */
	public static Logger logger = Logger.getLogger("AlreadyHaveHeroException");

	public AlreadyHaveHeroException(Long id) {
		logger.warning(id + "a deja un hero");
	}

}
