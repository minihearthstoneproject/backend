package univnantes.hearthstone.exception;

import java.util.logging.Logger;

import javax.validation.constraints.NotNull;

public class UnknowHeroException extends Exception {

	/**
		 * 
		 */
	private static final long serialVersionUID = -6801595492109976570L;
	public static Logger logger = Logger.getLogger("UnknowHeroException");

	public UnknowHeroException(@NotNull String name) {
		logger.warning("le hero " + name + " est inconnue");
	}

	public UnknowHeroException() {
		logger.warning("la hero  est inconnue");
	}
}
