package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;

@Entity
public class SpecialActionBenedictionDePuissance extends SpecialAction implements InvokeAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		((Servant) target).addAttack(3);
		sauvegarde.save(target);
	}

}
