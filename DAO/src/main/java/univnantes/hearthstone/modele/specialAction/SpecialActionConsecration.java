package univnantes.hearthstone.modele.specialAction;

import java.util.List;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;

@Entity
public class SpecialActionConsecration extends SpecialAction implements InvokeAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub


		Player player = (game.getPlayerTurn()==1) ? game.getPlayer2() : game.getPlayer1();
		List<univnantes.hearthstone.modele.Entity> list = player.getPlayingCard();
		list.add(player.getHero());

		for (univnantes.hearthstone.modele.Entity e : list) {
			e.addDamage(2);
			sauvegarde.save(e);
		}
	}
}
