package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;

@Entity
public class SpecialActionVolDeVie extends SpecialAction implements CurrentAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		target.addDamage(((Servant) excecutor).getAttack());
		if(game.getPlayerTurn()==1)
			game.getPlayer1().getHero().addHealthPoint(((Servant) excecutor).getAttack());
		else
			game.getPlayer2().getHero().addHealthPoint(((Servant) excecutor).getAttack());


		sauvegarde.save(target);
		sauvegarde.save(game);
		
	}

}
