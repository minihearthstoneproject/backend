package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;


@Entity
public class SpecialActionMage extends SpecialAction implements CurrentAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		target.addDamage(1);
		sauvegarde.save(target);
		
	}

}
