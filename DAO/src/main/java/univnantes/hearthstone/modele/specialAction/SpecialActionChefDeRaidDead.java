package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.specialAction.InvokeAction;

import java.util.ArrayList;
import java.util.List;

@Entity
public class SpecialActionChefDeRaidDead extends SpecialAction implements DeadAction{

    @Override
    public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
                        univnantes.hearthstone.modele.Entity target) {
        // TODO Auto-generated method stub

        List<univnantes.hearthstone.modele.Entity>  list = new ArrayList<>();

        if(game.getPlayerTurn()==2) {
            list = game.getPlayer1().getPlayingCard();
            for (univnantes.hearthstone.modele.Entity e : list) {
                if (e instanceof Servant) {
                    ((Servant) e).addAttack(-1);
                    sauvegarde.save(e);
                }
            }
        }
        else
        {
            list = game.getPlayer2().getPlayingCard();
            for (univnantes.hearthstone.modele.Entity e : list) {
                if (e instanceof Servant) {
                    ((Servant) e).addAttack(-1);
                    sauvegarde.save(e);
                }
            }
        }
    }
}
