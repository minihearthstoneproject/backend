package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;
import javax.persistence.criteria.CriteriaBuilder;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.specialAction.InvokeAction;

@Entity
public class SpecialActionProvocation extends SpecialAction implements InvokeAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		((Servant) excecutor).setProvocation(true);
		sauvegarde.save(excecutor);
	}

}
