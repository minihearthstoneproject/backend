package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.factories.CardFactory;

@Entity
public class SpecialActionImageMiroir extends SpecialAction {
	
	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) throws UnknowCardException {

		univnantes.hearthstone.modele.Entity servant1 = new CardFactory().createCard(CardFactory.IMAGE_MIROIR_SERVANT);
		univnantes.hearthstone.modele.Entity servant2 = new CardFactory().createCard(CardFactory.IMAGE_MIROIR_SERVANT);

		sauvegarde.save(servant1);
		sauvegarde.save(servant2);

		Player player = (game.getPlayerTurn()==1) ? game.getPlayer1() : game.getPlayer2();

		player.addPlayingCard(servant1);
		player.addPlayingCard(servant2);

		sauvegarde.save(player);

	}

}
