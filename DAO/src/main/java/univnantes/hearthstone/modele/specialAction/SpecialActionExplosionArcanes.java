package univnantes.hearthstone.modele.specialAction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;

@Entity
public class SpecialActionExplosionArcanes extends SpecialAction implements InvokeAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		List<univnantes.hearthstone.modele.Entity> list = (game.getPlayerTurn()==1) ? game.getPlayer2().getPlayingCard() : game.getPlayer1().getPlayingCard();

		for (univnantes.hearthstone.modele.Entity e : list) {
			e.addDamage(1);
			sauvegarde.save(e);
		}
	}
}
