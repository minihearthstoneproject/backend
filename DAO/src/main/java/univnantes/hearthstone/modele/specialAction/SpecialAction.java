package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;

@Entity
public abstract class SpecialAction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSpeAct;
	
    public abstract void execute( Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,  univnantes.hearthstone.modele.Entity target)
            throws UnknowCardException, UnknowHeroException;
    
    public SpecialAction() {
    	
    }
}
