package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.factories.CardFactory;

import java.util.ArrayList;
import java.util.Random;

@Entity
public class SpecialActionMaitriseDuBlocage extends SpecialAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) throws UnknowHeroException{

        if(game.getPlayerTurn()==1)
            game.getPlayer1().getHero().addArmor(5);
        else
            game.getPlayer2().getHero().addArmor(5);

        //recuperation des cartes jouables du joueur du tour courrant
        ArrayList<String> cartesJouables = new ArrayList<String>();
        Player player = (game.getPlayerTurn() == (byte) 1) ? game.getPlayer1() : game.getPlayer2();

        cartesJouables.addAll(CardFactory.getGeneralCards());
        cartesJouables.addAll(CardFactory.getSpecifiqueCards(player.getHero().getName()));

        //choix alleatoir de la carte dans la liste
        for(int i=0; i<2; i++) {
            Random r = new Random();
            int indiceCard = r.nextInt((cartesJouables.size() - 1));
            player.addHandCard(cartesJouables.get(indiceCard));
        }
        sauvegarde.save(player);
	}

}
