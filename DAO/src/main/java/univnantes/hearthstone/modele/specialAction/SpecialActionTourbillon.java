package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.specialAction.InvokeAction;

import java.util.ArrayList;
import java.util.List;

@Entity
public class SpecialActionTourbillon extends SpecialAction implements InvokeAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		List<univnantes.hearthstone.modele.Entity> list1 = game.getPlayer1().getPlayingCard();
		List<univnantes.hearthstone.modele.Entity> list2 = game.getPlayer2().getPlayingCard();

		for (univnantes.hearthstone.modele.Entity e: list1) {
			if(e instanceof Servant){
				e.addDamage(1);
				sauvegarde.save(e);
			}
		}
		for (univnantes.hearthstone.modele.Entity e: list2) {
			if (e instanceof Servant) {
				e.addDamage(1);
				sauvegarde.save(e);
			}
		}
	}
}
