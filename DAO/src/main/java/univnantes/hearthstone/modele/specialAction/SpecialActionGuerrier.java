package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;

@Entity
public class SpecialActionGuerrier extends SpecialAction implements CurrentAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) {
		// TODO Auto-generated method stub

		Player player = game.getPlayerTurn()==1 ? game.getPlayer1() : game.getPlayer2();
		player.getHero().addArmor(2);

		sauvegarde.save(player.getHero());
	}

}
