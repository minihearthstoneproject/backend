package univnantes.hearthstone.modele.specialAction;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Autowired;
import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.specialAction.InvokeAction;

import static univnantes.hearthstone.modele.factories.CardFactory.RECRUE_DE_LA_MAIN_D_ARGENT;

@Entity
public class SpecialActionPaladin extends SpecialAction implements CurrentAction {

	@Override
	public void execute(Sauvegarde sauvegarde, Game game, univnantes.hearthstone.modele.Entity excecutor,
			univnantes.hearthstone.modele.Entity target) throws UnknowCardException {

		univnantes.hearthstone.modele.Entity servant = new CardFactory().createCard(CardFactory.RECRUE_DE_LA_MAIN_D_ARGENT);
		sauvegarde.save(servant);
		if (game.getPlayerTurn() == 1){
			game.getPlayer1().addPlayingCard(servant);
			sauvegarde.save(game.getPlayer1());
		}
		else{
			game.getPlayer2().addPlayingCard(servant);
			sauvegarde.save(game.getPlayer2());
		}

		sauvegarde.save(servant);
	}

}
