package univnantes.hearthstone.modele;

import javax.persistence.*;


@javax.persistence.Entity
@Table(name = "ENTITY")
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn( name = "type" )
public abstract class Entity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEntity;

    @Column(name = "NAME")
    private String name;

    @Column(name = "HEALTH_POINT")
    protected int healthPoint;
    
	@Column(name = "CAN_ATTACK")
	private boolean canAttack;

    public Entity(){}

    public abstract void addDamage(int damage);

    public Entity(String name, int healthPoint, boolean canAttack) {
        this.name = name;
        this.healthPoint = healthPoint;
		this.canAttack = canAttack;
    }


    public Long getIdEntity() {
        return idEntity;
    }

    public void setIdEntity(Long idEntity) {
        this.idEntity = idEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public void addHealthPoint(int add){
        this.healthPoint+=add;
    }

	public boolean isCanAttack() {
		return canAttack;
	}

	public void setCanAttack(boolean canAttack) {
		this.canAttack = canAttack;
	}
}
