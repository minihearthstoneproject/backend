package univnantes.hearthstone.modele;

import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import univnantes.hearthstone.modele.specialAction.SpecialAction;

@Entity
@Table(name = "SERVANT")
public class Servant extends univnantes.hearthstone.modele.Entity implements Card {

	

	@Column(name = "INVOKE_COST")
	private int invokeCost;

	@Column(name = "DAMAGE")
	private int attack;

	@Column(name = "PROVOCATION")
	private boolean provocation;

	@LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @OneToMany(cascade=CascadeType.ALL)
    private List<SpecialAction> specialActions;

	public Servant() {
	}

	public Servant(String name, int invokeCost, int healthPoint, int attack, boolean provocation, boolean canAttack, List<SpecialAction> specialAction) {
		super(name, healthPoint, canAttack);
		this.invokeCost = invokeCost;
		this.attack = attack;
		this.provocation = provocation;
		this.specialActions = specialAction;
	}

	public List<SpecialAction> getSpecialAction() {
		return specialActions;
	}

	public void setSpecialAction(List<SpecialAction> specialAction) {
		this.specialActions = specialAction;
	}

	public int getInvokeCost() {
		return invokeCost;
	}

	public void setInvokeCost(int invokeCost) {
		this.invokeCost = invokeCost;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public boolean isProvocation() {
		return provocation;
	}

	public void setProvocation(boolean provocation) {
		this.provocation = provocation;
	}

	public void addAttack(int damage){ this.attack += damage; }

	@Override
	public void addDamage(int damage) {
		this.addHealthPoint(-damage);
	}
	
	@Override
	public String toString() {
		return "Servant [invokeCost=" + invokeCost + ", attack=" + attack + ", provocation=" + provocation
				+ ", canAttack=" + isCanAttack() + "]";
	}
}