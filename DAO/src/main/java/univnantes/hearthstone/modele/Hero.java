package univnantes.hearthstone.modele;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import univnantes.hearthstone.modele.specialAction.SpecialAction;


@Entity
@Table(name = "HERO")
public class Hero extends univnantes.hearthstone.modele.Entity {

	
	@Column(name = "ARMOR")
	private int armor;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	private SpecialAction specialAction;

	public Hero() {super();}
	public Hero(String name, int hp, int armor, SpecialAction specialAction, boolean canAttack) {
		super(name, hp, canAttack);
		this.armor = armor;
		this.specialAction = specialAction;
	}

	public Long getIdHero() {
		return getIdEntity();
	}

	public void setIdHero(Long idHero) {
		this.setIdEntity(idHero) ;
	}

	public int getHealPoint() {
		return getHealthPoint();
	}

	public void setHp(int hp) {
		this.setHealthPoint(hp);
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}
	
	public void addArmor(int add){ this.armor += add;}
	
	public SpecialAction getSpecialAction() {
		return specialAction;
	}
	
	public void setSpecialAction(SpecialAction specialAction) {
		this.specialAction = specialAction;
	}

	@Override
	public void addDamage(int damage){
		if(this.armor == 0)
			this.healthPoint-=damage;
		else if(armor>=damage)
			this.armor-=damage;
		else
		{
			damage -= this.armor;
			this.armor = 0;
			this.healthPoint -= damage;
		}
	}
	@Override
	public String toString() {
		return "Hero [idHero=" + getIdEntity() + ", name=" + getName() + ", hp=" + getHealPoint() + ", armor=" + armor + "]";
	}
}
