package univnantes.hearthstone.modele;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;


@Entity
@Table(name = "PLAYER")
public class Player {
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPlayer;
	
	@Column(name = "KEY_PLAYER")
	private String key;
	
	@Column(name = "NAME")
	private String name;

	@OneToOne(cascade=CascadeType.PERSIST)
    private Hero hero;

	@LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    private List<String> hand;

	@LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection
    @CollectionTable(name = "PLAYER_PLAYING_CARD", joinColumns = @JoinColumn(name="PLAYER_idPlayer"))
    @Column(name = "PLAYING_CARD")
    private List<univnantes.hearthstone.modele.Entity> playingCard;
    
    @Column(name = "MANA")
    private int mana;
    
    @Column(name = "MAX_MANA")
    private int maxMana;

    public Player() {}
	public Player(String name, Hero hero, List<String> hand, List<univnantes.hearthstone.modele.Entity> l, int mana, int maxMana, String key) {
		super();
		this.name = name;
		this.hero = hero;
		this.hand = hand;
		this.playingCard = l;
		this.key = key;
		this.mana = mana;
		this.maxMana = maxMana;
		
	}
	
	public void addPlayingCard(univnantes.hearthstone.modele.Entity servant) {
		playingCard.add(servant);
	}

	public void removePlayingCard(Servant servant) {
		playingCard.remove(servant);
	}
	
	
	public Long getIdPlayer() {
		return idPlayer;
	}

	public void setIdPlayer(Long idPlayer) {
		this.idPlayer = idPlayer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Hero getHero() {
		return hero;
	}

	public void setHero(Hero hero) {
		this.hero = hero;
	}

	public List<String> getHand() {
		return hand;
	}

	public void setHand(List<String> hand) {
		this.hand = hand;
	}

	public List<univnantes.hearthstone.modele.Entity> getPlayingCard() {
		return playingCard;
	}

	public void setPlayingCard(List<univnantes.hearthstone.modele.Entity> playingCard) {
		this.playingCard = playingCard;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getMaxMana() {
		return maxMana;
	}

	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}

	public void incremMaxMana(){ this.maxMana += 1; }

	@Override
	public String toString() {
		return "Player [idPlayer=" + idPlayer + ", name=" + name + ", hero=" + hero + ", hand=" + hand
				+ ", playingCard=" + playingCard + ", mana=" + mana + ", maxMana=" + maxMana + "]";
	}
	public void removeHandCard(String name) {
		hand.remove(name);	
	}
    
	public void addHandCard(String name) {
		hand.add(name);	
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
    
	
}
