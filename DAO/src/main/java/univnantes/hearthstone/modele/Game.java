package univnantes.hearthstone.modele;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "GAME")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idGame;

	@OneToOne(cascade=CascadeType.MERGE)
	private Player player1;

	@OneToOne(cascade=CascadeType.MERGE)
	private Player player2;

	@Column(name = "PLAYER_TURN")
	private byte playerTurn;

	@Column(name = "KEY_GAME")
	private String key;
	
	@Column(name = "DATE_GAME")
	private Long date;

	public Game() {}
	public Game(Player Player1, Player Player2, byte playerTurn, Long date, String key) {
		super();
		this.player1 = Player1;
		this.player2 = Player2;
		this.playerTurn = playerTurn;
		this.key = key;
		this.date = date;
	}

	public Long getIdGame() {
		return idGame;
	}

	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}

	public byte getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(byte playerTurn) {
		this.playerTurn = playerTurn;
	}

	public String getkey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public Long getdate() {
		return this.date;
	}

	public void setdate(Long date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Game [idGame=" + idGame + ", Player1=" + player1 + ", Player2=" + player2 + ", playerTurn="
				+ playerTurn + ", key=" + key + ", date=" + date + "]";
	}

}
