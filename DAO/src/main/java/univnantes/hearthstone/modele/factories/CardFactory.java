package univnantes.hearthstone.modele.factories;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.Spell;
import univnantes.hearthstone.modele.specialAction.*;

@Component
public class CardFactory {

	/**
	 * nom des cartes communes
	 */
	public static final String SANGLIER_BROCHEROC = "sanglier brocheroc";
	public static final String SOLDAT_DU_COMPTE_DE_L_OR = "soldat du compte de l or";
	public static final String CHEVAUCHEUR_DE_LOUP = "chevaucheur de loup";
	public static final String CHEF_DE_RAID = "chef de raid";
	public static final String YETI_NOROIT = "yeti noroit";
	public static final List<String> getGeneralCards() {
		List<String> l = new ArrayList<String>();
		l.add(SANGLIER_BROCHEROC);
		l.add(SOLDAT_DU_COMPTE_DE_L_OR);
		l.add(CHEVAUCHEUR_DE_LOUP);
		l.add(CHEF_DE_RAID);
		l.add(YETI_NOROIT);
		return l;
	}
	/**
	 * nom des cartes spécifique au mage
	 */
	public static final String IMAGE_MIROIR = "image miroir";
	public static final String IMAGE_MIROIR_SERVANT = "image miroir servant";
	public static final String EXPLOSION_DES_ARCANES = "explosion des arcanes";
	public static final String METAMORPHOSE = "metamorphose";
	public static final List<String> getMageCards() {
		List<String> l = new ArrayList<String>();
		l.add(IMAGE_MIROIR);
		l.add(EXPLOSION_DES_ARCANES);
		l.add(METAMORPHOSE);
		return l;
	}
	/**
	 * nom des cartes spécifique au paladin
	 */
	public static final String CHAMPION_FRISSELAME = "champion frisselame";
	public static final String BENEDICTION_DE_PUISSANCE = "benediction de puissance";
	public static final String CONSECRATION = "consecration";
	public static final String RECRUE_DE_LA_MAIN_D_ARGENT = "recrue de la main d argent";
	public static final List<String> getPaladinCards() {
		List<String> l = new ArrayList<String>();
		l.add(CHAMPION_FRISSELAME);
		l.add(BENEDICTION_DE_PUISSANCE);
		l.add(CONSECRATION);
		l.add(RECRUE_DE_LA_MAIN_D_ARGENT);
		return l;
	}
	/**
	 * nom des cartes spécifique au guerrier
	 */
	public static final String TOURBILLON = "tourbillon";
	public static final String AVOCAT_COMMIS_D_OFFICE = "avocat commis d office";
	public static final String MAITRISE_DU_BLOCAGE = "maitrise du blocage";
	public static final List<String> getGuerrierCards() {
		List<String> l = new ArrayList<String>();
		l.add(TOURBILLON);
		l.add(AVOCAT_COMMIS_D_OFFICE);
		l.add(MAITRISE_DU_BLOCAGE);
		return l;
	}
	
	public static final List<String> getSpecifiqueCards(String name) throws UnknowHeroException {
		switch(name) {
		case HeroFactory.GUERRIER:
			return getGuerrierCards();
		case HeroFactory.PALADIN:
			return getPaladinCards();
		case HeroFactory.MAGE:
			return getMageCards();
		default:
			throw new UnknowHeroException(name);
		}
	}
	
	public CardFactory() {
	}

	public Entity createCard(@NotNull String name) throws UnknowCardException {
		List<SpecialAction> specialActions = new ArrayList<SpecialAction>();
		switch (name) {
		case SANGLIER_BROCHEROC:
			return new Servant(SANGLIER_BROCHEROC, 1, 1, 1, false, false, specialActions);
		case SOLDAT_DU_COMPTE_DE_L_OR:
			specialActions.add(new SpecialActionProvocation());
			return new Servant(SOLDAT_DU_COMPTE_DE_L_OR, 1, 2, 1, false, false, specialActions);
		case CHEVAUCHEUR_DE_LOUP:
			specialActions.add(new SpecialActionCharge());
			return new Servant(CHEVAUCHEUR_DE_LOUP, 3, 1, 3, false, false, specialActions);
		case CHEF_DE_RAID:
			specialActions.add(new SpecialActionChefDeRaidInvoke());
			specialActions.add(new SpecialActionChefDeRaidDead());
			return new Servant(CHEF_DE_RAID, 3, 2, 2, false, false, specialActions);
		case YETI_NOROIT:
			return new Servant(YETI_NOROIT, 4, 5, 4, false, false, specialActions);
		case CHAMPION_FRISSELAME:
			specialActions.add(new SpecialActionVolDeVie());
			return new Servant(CHAMPION_FRISSELAME, 4, 2, 3, false, false, specialActions);
		case AVOCAT_COMMIS_D_OFFICE:
			specialActions.add(new SpecialActionProvocation());
			return new Servant(AVOCAT_COMMIS_D_OFFICE, 2, 7, 0, false, false, specialActions);
		case RECRUE_DE_LA_MAIN_D_ARGENT:
			return new Servant(RECRUE_DE_LA_MAIN_D_ARGENT, 0, 1, 1, false, false, specialActions);
		case IMAGE_MIROIR_SERVANT:
			return new Servant(IMAGE_MIROIR_SERVANT, 0, 2, 0, true, false, null);
		case IMAGE_MIROIR:
			return new Spell(IMAGE_MIROIR, 1, new SpecialActionImageMiroir());
		case EXPLOSION_DES_ARCANES:
			return new Spell(EXPLOSION_DES_ARCANES, 2, new SpecialActionExplosionArcanes());
		case METAMORPHOSE:
			return new Spell(METAMORPHOSE, 4, new SpecialActionMetamorphose());
		case BENEDICTION_DE_PUISSANCE:
			return new Spell(BENEDICTION_DE_PUISSANCE, 1, new SpecialActionBenedictionDePuissance());
		case CONSECRATION:
			return new Spell(CONSECRATION, 4, new SpecialActionConsecration());
		case TOURBILLON:
			return new Spell(TOURBILLON, 1, new SpecialActionTourbillon());
		case MAITRISE_DU_BLOCAGE:
			return new Spell(MAITRISE_DU_BLOCAGE, 3, new SpecialActionMaitriseDuBlocage());
		default:
			throw new UnknowCardException(name);
		}
	}

}
