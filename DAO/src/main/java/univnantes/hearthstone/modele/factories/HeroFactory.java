package univnantes.hearthstone.modele.factories;

import java.util.Arrays;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.specialAction.SpecialActionGuerrier;
import univnantes.hearthstone.modele.specialAction.SpecialActionMage;
import univnantes.hearthstone.modele.specialAction.SpecialActionPaladin;

@Component
public class HeroFactory {

	/**
	 * nom des hero
	 */
	public static final String MAGE = "mage";
	public static final String PALADIN = "paladin";
	public static final String GUERRIER = "guerrier";
	
	/**
	 * valeur du hp et de l'armur lors de la creation d'un hero
	 */
	public static final int HP_INIT = 30;
	public static final int ARMOR_INIT = 0;
	
	/**
	 * carte commune a tous les heros
	 */
	public static final List<String> COMMON_CARDS = Arrays.asList(CardFactory.SANGLIER_BROCHEROC,
			CardFactory.SOLDAT_DU_COMPTE_DE_L_OR, CardFactory.CHEVAUCHEUR_DE_LOUP, CardFactory.CHEF_DE_RAID, CardFactory.YETI_NOROIT);
	
	/**
	 * carte spécifiques a chaque hero
	 */
	public static final List<String> SPECIAL_CARDS_OF_MAGE = Arrays.asList(CardFactory.IMAGE_MIROIR,
			CardFactory.EXPLOSION_DES_ARCANES, CardFactory.METAMORPHOSE);
	public static final List<String> SPECIAL_CARDS_OF_PALADIN = Arrays.asList(CardFactory.CHAMPION_FRISSELAME,
			CardFactory.BENEDICTION_DE_PUISSANCE, CardFactory.CONSECRATION);
	public static final List<String> SPECIAL_CARDS_OF_GUERRIER = Arrays.asList(CardFactory.TOURBILLON,
			CardFactory.AVOCAT_COMMIS_D_OFFICE, CardFactory.MAITRISE_DU_BLOCAGE);

	
	
	public HeroFactory() {}
	
	/**
	 * permet la creation d'un hero
	 * @param name
	 * @return
	 * @throws UnknowHeroException
	 */
	public Hero createHero(@NotNull String name) throws UnknowHeroException {
		switch (name) {
		case MAGE:
			return new Hero(MAGE, HP_INIT, ARMOR_INIT, new SpecialActionMage(), true);
		case PALADIN:
			return new Hero(PALADIN, HP_INIT, ARMOR_INIT, new SpecialActionPaladin(), true);
		case GUERRIER:
			return new Hero(GUERRIER, HP_INIT, ARMOR_INIT, new SpecialActionGuerrier(), true);
		default:
			throw new UnknowHeroException(name);
		}
	}

}
