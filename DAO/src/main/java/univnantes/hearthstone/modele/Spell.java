package univnantes.hearthstone.modele;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import univnantes.hearthstone.modele.specialAction.SpecialAction;

@Entity
@Table(name = "SPELL")
public class Spell extends univnantes.hearthstone.modele.Entity implements Card{
	
	@Column(name = "USE_COST")
	private int useCost;

	@OneToOne(cascade=CascadeType.PERSIST)
	private SpecialAction specialAction;

	public Spell() {}


	public Spell(String name, int useCost, SpecialAction specialAction) {
		super(name, 0, true);
		this.useCost = useCost;
		this.specialAction = specialAction;
	}

	public SpecialAction getSpecialAction() {
		return specialAction;
	}
	public void setSpecialAction(SpecialAction specialAction) {
		this.specialAction = specialAction;
	}

	public int getUseCost() {
		return useCost;
	}

	public void setUseCost(int useCost) {
		this.useCost = useCost;
	}

	@Override
	public void addDamage(int damage) { }
}
