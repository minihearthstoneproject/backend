package univnantes.hearthstone.dao.util;

import java.util.ArrayList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.factories.CardFactory;

@Component
public class PiocheManager {

	@Autowired
	private IDaoServicePlayer daoServicePlayer;

	/**
	 * gerer l'action pioche à chaque début de tour: une carte C C appartient au
	 * carte jouable du joueur (cartes specifiques du hero, plus les cartes
	 * générales)
	 * 
	 * @param game
	 * @throws UnknowHeroException 
	 */
	public void piocheOneCard(Game game) throws UnknowHeroException {
		//recuperation des cartes jouables du joueur du tour courrant
		ArrayList<String> cartesJouables = new ArrayList<String>();
		Player player = (game.getPlayerTurn() == (byte) 1) ? game.getPlayer1() : game.getPlayer2();
		cartesJouables.addAll(CardFactory.getGeneralCards());
		cartesJouables.addAll(CardFactory.getSpecifiqueCards(player.getHero().getName()));
		
		//choix alleatoir de la carte dans la liste
		Random r = new Random();
		int indiceCard = r.nextInt((cartesJouables.size()-1));
			
		daoServicePlayer.addHandCard(player, cartesJouables.get(indiceCard));
	}

	/**
	 * distribution des premieres cartes
	 * @param game
	 * @throws UnknowHeroException 
	 */
	public void piocheInitGame(Game game) throws UnknowHeroException {
		//recuperation des cartes jouables du joueur du tour courrant
		ArrayList<String> cartesJouablesFirstPlayer = new ArrayList<String>();
		ArrayList<String> cartesJouablesSecPlayer = new ArrayList<String>();

		Player playerFirst;
		Player playerSec;
		if(game.getPlayerTurn() == (byte) 1) {
			playerFirst = game.getPlayer1(); 
			playerSec = game.getPlayer2();
		}else {
			playerSec = game.getPlayer1(); 
			playerFirst = game.getPlayer2();
		}
		
		//cartes du premier joueur
		cartesJouablesFirstPlayer.addAll(CardFactory.getGeneralCards());
		cartesJouablesFirstPlayer.addAll(CardFactory.getSpecifiqueCards(playerFirst.getHero().getName()));
		
		//choix alleatoir des 3 cartes dans la liste
		Random r = new Random();
		int indiceCard1 = r.nextInt((cartesJouablesFirstPlayer.size()-1));
		int indiceCard2 = r.nextInt((cartesJouablesFirstPlayer.size()-1));
		int indiceCard3 = r.nextInt((cartesJouablesFirstPlayer.size()-1));
		playerFirst.addHandCard(cartesJouablesFirstPlayer.get(indiceCard1));
		playerFirst.addHandCard(cartesJouablesFirstPlayer.get(indiceCard2));
		playerFirst.addHandCard(cartesJouablesFirstPlayer.get(indiceCard3));
		
		//cartes du deuxieme joueur
		cartesJouablesSecPlayer.addAll(CardFactory.getGeneralCards());
		cartesJouablesSecPlayer.addAll(CardFactory.getSpecifiqueCards(playerSec.getHero().getName()));
				
		//choix alleatoir des 3 cartes dans la liste
		indiceCard1 = r.nextInt((cartesJouablesSecPlayer.size()-1));
		indiceCard2 = r.nextInt((cartesJouablesSecPlayer.size()-1));
		indiceCard3 = r.nextInt((cartesJouablesSecPlayer.size()-1));
		playerSec.addHandCard(cartesJouablesSecPlayer.get(indiceCard1));
		playerSec.addHandCard(cartesJouablesSecPlayer.get(indiceCard2));
		playerSec.addHandCard(cartesJouablesSecPlayer.get(indiceCard3));
		
	}
}
