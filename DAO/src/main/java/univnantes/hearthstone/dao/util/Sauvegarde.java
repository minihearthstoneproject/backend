package univnantes.hearthstone.dao.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;

@Component
public class Sauvegarde {
	
	@Autowired
	EntityRepository entityRepo;

	@Autowired
	GameRepository gameRepo;
	
	@Autowired
	PlayerRepository playerRepo;
	
	public void save(Object o) {
		if(o instanceof Entity) {
			entityRepo.save((Entity) o);
		}
		if(o instanceof Game) {
			gameRepo.save((Game) o);
		}
		if(o instanceof Player) {
			playerRepo.save((Player) o);
		}		
	}
}
