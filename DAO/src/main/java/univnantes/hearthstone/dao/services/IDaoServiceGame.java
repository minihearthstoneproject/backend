package univnantes.hearthstone.dao.services;

import javax.validation.constraints.NotNull;

import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;

public interface IDaoServiceGame {

	/**
	 * creer une game avec le joueur idplayer
	 * @param gameKey 
	 * @param player1
	 * @return
	 */
	Game createGameWithPlayer(String gameKey, @NotNull Player player1);
	
	/**
	 * enregistre la game
	 * @param game
	 * @return
	 */
	Game saveGame(@NotNull Game game);

	/**
	 * permet d'ajouter le joueur a la game
	 * @param idGame
	 * @param player2
	 * @return
	 */
	Game joinGameWithId(@NotNull Long idGame,@NotNull Player player2);

	/**
	 * return true si il y a deja 2 joueur false sinon
	 * @param idGame
	 * @return
	 */
	boolean isFull(@NotNull Long idGame);

	/**
	 * retourn la game avec l'id correspondant
	 * @param idGame
	 * @return
	 */
	Game get(@NotNull Long idGame);
	
	/**
	 * supprime la game avec l'id correspondant
	 * @param idGame
	 * @return
	 */
	void delete(@NotNull Long idGame);
	
	/**
	 * retourn l'entite avec l'id correspondant
	 * @param idEntity
	 * @return
	 */
	Entity getEntityById(@NotNull Long idEntity);

	/**
	 * timer du tour sonne. Il faut passer au joueur suivant
	 * @param idGame
	 */
	Game turnTimeOut(@NotNull Long idGame);

	/**
	 * supprime dans la base de données tous ce qui a en rapport avec cette partie
	 * @param game
	 */
	void deleteGame(Game game);

	/**
	 * actualise le temps de lancement du nouveau timer
	 * @param idGame
	 */
	void setTimeTurn(Long idGame);
}
