package univnantes.hearthstone.dao.services.impl;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

@Component
public class DaoServiceGame implements IDaoServiceGame {

	public static final byte FIRST_PLAYER_INIT = 1;

	@Autowired
	GameRepository gameRepo;

	@Autowired
	EntityRepository entityRepo;

	@Override
	public Game createGameWithPlayer(String gameKey, @NotNull Player player1) {
		Game game = new Game(player1, null, FIRST_PLAYER_INIT, null, gameKey);
		return saveGame(game);
	}

	@Override
	public Game saveGame(@NotNull Game game) {
		return gameRepo.save(game);
	}

	@Override
	public Game joinGameWithId(@NotNull Long idGame, @NotNull Player player2) {
		gameRepo.setPlayer2OfIdGame(idGame, player2);
		return gameRepo.findByIdGame(idGame);
	}

	@Override
	public boolean isFull(@NotNull Long idGame) {
		return gameRepo.findByIdGame(idGame).getPlayer2() != null;
	}

	@Override
	public Game get(@NotNull Long idGame) {
		return gameRepo.findByIdGame(idGame);
	}

	@Override
	public void delete(@NotNull Long idGame) {
		gameRepo.deleteById(idGame);
	}

	@Override
	public Entity getEntityById(@NotNull Long idEntity) {
		Entity entity = entityRepo.findByIdEntity(idEntity);
		if (entity instanceof Hero) {
			return (Hero) entity;
		} else {
			return (Servant) entity;
		}
	}

	@Override
	public Game turnTimeOut(@NotNull Long idGame) {
		// definition du nouveau joueur pour le tour suivant
		gameRepo.setPlayerTurnOfIdGame(idGame, inversePlayerTurn(idGame));
		return gameRepo.findByIdGame(idGame);
	}

	/**
	 * inverse le tour
	 * 
	 * @param idGame
	 * @return 2 si game.playerTurn == 1 sinon 1
	 */
	private byte inversePlayerTurn(@NotNull Long idGame) {
		return (byte) (gameRepo.findByIdGame(idGame).getPlayerTurn() == ((byte) 1) ? 2 : 1);
	}

	@Override
	public void deleteGame(Game game) {
		gameRepo.deleteByIdGame(game.getIdGame());		
	}

	@Override
	public void setTimeTurn(Long idGame) {
		gameRepo.setDateOfIdGame(idGame, new Date().getTime());
	}

}
