package univnantes.hearthstone.dao.services;

import javax.validation.constraints.NotNull;

import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

public interface IDaoServicePlayer {
	
	/**
	 * creer un player
	 * @param name
	 * @return
	 */
	Player createPlayer(String name, String key) ;

	/**
	 * affectation du hero a la personne avec cet id
	 * 
	 * @param idUser
	 * @param hero
	 */
	void setHero(@NotNull Long idUser, @NotNull Hero hero);

	/**
	 * verifie si la personne avec cet id a deja un hero
	 * @param idUser
	 * @return
	 */
	boolean alreadyHaveHero(@NotNull Long idUser);

	/**
	 * ajoute la carte dans playing card et la supprime de la main
	 * 
	 * @param player
	 * @param servant
	 */
	void invokeCard(@NotNull Player player,@NotNull Entity servant);

	/**
	 * enleve au jooueur le nombre de mana passer en parametre
	 * @param player
	 * @param invokeCost
	 */
	void StealManaPlayer(Player player, int invokeCost);

	/**
	 * ajoute la carte a la main du joueur
	 * @param player
	 * @param card
	 * @return 
	 */
	Player addHandCard(Player player, String card);

	/**
	 * supprime la carte de la main du joueur
	 * @param player
	 * @param card
	 */
	void removeHandCard(Player player, Entity card);

	/**
	 * sauvegarde le joueur
	 * @param player
	 */
	Player save(Player player);

	/**
	 * supprime la carte du plateau
	 * @param player
	 */

	Player deleteServant(Player player, Servant servant);

}
