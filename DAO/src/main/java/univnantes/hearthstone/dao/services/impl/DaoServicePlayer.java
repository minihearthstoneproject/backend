package univnantes.hearthstone.dao.services.impl;

import java.util.ArrayList;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

@Component
public class DaoServicePlayer implements IDaoServicePlayer{

	public static final int MAX_MANA_PLAYER_INIT = 10;
	public static final int MANA_PLAYER_INIT = 0;
	public static final String ANONYME = "anonyme";

	@Autowired
	PlayerRepository playerRepo;

	@Autowired
	EntityRepository entityRepo;

	@Override
	public Player createPlayer(String name, String key) {
		String playerName = (name == null) ? ANONYME : name;

		Player player = new Player(playerName, null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), MANA_PLAYER_INIT, MANA_PLAYER_INIT, key);
		playerRepo.save(player);
		return player;
	}

	@Override
	public void setHero(@NotNull Long idUser, @NotNull Hero hero) {
		entityRepo.save(hero);
		playerRepo.setHeroOfIdPlayer(idUser, hero);
	}

	@Override
	public boolean alreadyHaveHero(@NotNull Long idUser) {
		return playerRepo.findByIdPlayer(idUser).getHero() != null;
	}

	@Override
	public void invokeCard(@NotNull Player player,@NotNull Entity servant) {
		entityRepo.save(servant);
		player.addPlayingCard(servant);
		player.removeHandCard(servant.getName());
		playerRepo.save(player);
	}

	@Override
	public void StealManaPlayer(Player player, int invokeCost) {
		int mana = player.getMana();
		player.setMana(mana - invokeCost);
		playerRepo.save(player);
	}

	@Override
	public Player addHandCard(Player player, String card) {
		player.addHandCard(card);
		return playerRepo.save(player);
	}

	@Override
	public void removeHandCard(Player player, Entity card) {
		player.removeHandCard(card.getName());
		playerRepo.save(player);
	}

	@Override
	public Player save(Player player) {
		return playerRepo.save(player);
	}

	@Override
	public Player deleteServant(Player player, Servant servant) {
		//entityRepo.delete(servant);
		player.removePlayingCard(servant);
		return playerRepo.save(player); 
	}

}
