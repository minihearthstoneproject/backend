package univnantes.hearthstone.dao.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.services.IDaoServiceEntity;
import univnantes.hearthstone.modele.Entity;

@Component
public class DaoServiceEntity implements IDaoServiceEntity{

	@Autowired
	EntityRepository entityRepo;

	
	@Override
	public Entity getEntity(Long idEntity) {
		return entityRepo.findByIdEntity(idEntity);
	}


	@Override
	public boolean isDead(Long idTarget) {
		return entityRepo.findByIdEntity(idTarget).getHealthPoint() <= 0;
	}


	@Override
	public Entity canAttackTrue(Entity entity) {
		entity.setCanAttack(true);
		return entityRepo.save(entity);
	}

}
