package univnantes.hearthstone.dao.services;

import univnantes.hearthstone.modele.Entity;

public interface IDaoServiceEntity {

	Entity getEntity(Long idEntity);

	boolean isDead(Long idTarget);

	Entity canAttackTrue(Entity entity);
}
