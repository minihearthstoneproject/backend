package univnantes.hearthstone.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.specialAction.SpecialAction;


@Transactional(readOnly = true)
public interface EntityRepository extends JpaRepository<Entity, Long> {

    List<Entity> findAll();

    Entity findByIdEntity(Long idEntity);
    

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.canAttack = ?2 where s.idEntity = ?1")
    void setCanAttackOfIdEntity(Long idEntity, boolean canAttack);
  

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity h set h.name = ?2 where h.idEntity = ?1")
    void setNameOfIdHero(Long idEntity, String name);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity h set h.armor = ?2 where h.idEntity = ?1")
    void setArmorOfIdHero(Long idEntity, int armor);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity h set h.armor = h.armor+?2 where h.idEntity = ?1")
    void addArmorOfIdHero(Long idEntity, int armor);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity h set h.specialAction = ?2 where h.idEntity = ?1")
    void setSpecialActionOfIdHero(Long idEntity, SpecialAction specialAction);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from Entity h where h.idEntity = ?1")
    void deleteByIdHero(Long idEntity);

    /*-----------------------------------------------------------------------------------------------------*/

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.name = ?2 where s.idEntity = ?1")
    void setNameOfIdServant(Long idEntity, String name);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.invokeCost = ?2 where s.idEntity = ?1")
    void setInvokeCostOfIdServant(Long idEntity, int invokeCost);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.healthPoint = ?2 where s.idEntity = ?1")
    void setHealthPointOfIdServant(Long idEntity, int healthPoint);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.healthPoint = s.healthPoint+?2 where s.idEntity = ?1")
    void addHealthPointOfIdEntity(Long idEntity, int healthPoint);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.provocation = ?2 where s.idEntity = ?1")
    void setProvocationOfIdEntity(Long idEntity, boolean provocation);


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Entity s set s.specialAction = ?2 where s.idEntity = ?1")
    void setSpecialActionOfIdServant(Long idEntity, SpecialAction specialAction);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("delete from Entity s where s.idEntity = ?1")
    void deleteByIdServant(Long idEntity);
    
   
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Entity s set s.name = ?2 where s.idEntity = ?1")
	void setNameOfIdSpell(Long idSpell, String name);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Entity s set s.useCost = ?2 where s.idEntity = ?1")
	void setUseCostOfIdSpell(Long idSpell, int useCost);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Entity s set s.specialAction = ?2 where s.idEntity = ?1")
	void setSpecialActionOfIdSpell(Long idSpell, SpecialAction specialAction);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete from Entity s where s.idEntity = ?1")
	void deleteByIdSpell(Long idSpell);
    
}

