package univnantes.hearthstone.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;

@Transactional(readOnly = true)
public interface PlayerRepository extends JpaRepository<Player, Long> {

	List<Player> findAll();

	Player findByIdPlayer(Long idPlayer);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.hero = ?2 where p.idPlayer = ?1")
	void setHeroOfIdPlayer(Long idPlayer, Hero hero);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.hand = ?2 where p.idPlayer = ?1")
	void setHandOfIdPlayer(Long idPlayer, List<String> hand);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.name = ?2 where p.idPlayer = ?1")
	void setNameOfIdPlayer(Long idPlayer, String hand);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.playingCard = ?2 where p.idPlayer = ?1")
	void setPlayingCardOfIdPlayer(Long idPlayer, List<Entity> playingCard);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.mana = ?2 where p.idPlayer = ?1")
	void setManaOfIdPlayer(Long idPlayer, int mana);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.key = ?2 where p.idPlayer = ?1")
	void setKeyOfIdPlayer(Long idPlayer, String key);
	
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Player p set p.maxMana = ?2 where p.idPlayer = ?1")
	void setMaxManaOfIdPlayer(Long idPlayer, int maxMana);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete from Player p where p.idPlayer = ?1")
	void deleteByIdPlayer(Long idPlayer);
}
