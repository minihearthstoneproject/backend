package univnantes.hearthstone.dao.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;

@Transactional(readOnly = true)
public interface GameRepository extends JpaRepository<Game, Long> {

	List<Game> findAll();

	Game findByIdGame(Long idGame);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Game g set g.player1 = ?2 where g.idGame = ?1")
	void setPlayer1OfIdGame(Long idGame, Player player1);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Game g set g.player2 = ?2 where g.idGame = ?1")
	void setPlayer2OfIdGame(Long idGame, Player player2);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Game g set g.key = ?2 where g.idGame = ?1")
	void setKeyOfIdGame(Long idGame, String key);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Game g set g.date = ?2 where g.idGame = ?1")
	void setDateOfIdGame(Long idGame, Long date);
	
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Game g set g.playerTurn = ?2 where g.idGame = ?1")
	void setPlayerTurnOfIdGame(Long idGame, byte playerTurn);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("delete from Game g where g.idGame = ?1")
	void deleteByIdGame(Long idGame);
}
