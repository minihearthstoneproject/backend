package univnantes.hearthstone;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.Spell;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.modele.specialAction.SpecialActionGuerrier;
import univnantes.hearthstone.modele.specialAction.SpecialActionMage;
import univnantes.hearthstone.modele.specialAction.SpecialActionPaladin;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static univnantes.hearthstone.modele.factories.CardFactory.*;
import static univnantes.hearthstone.modele.factories.HeroFactory.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("univnantes.hearthstone")
public class FactoriesTest {


    @Test
    public void createHeroTestException() throws UnknowHeroException{
        boolean thrown = false;

        try {
            Entity hero = new HeroFactory().createHero("Error");
        } catch (UnknowHeroException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }


    @DisplayName("createHeroTestValid")
    @ParameterizedTest
    @ValueSource(strings = {MAGE, PALADIN, GUERRIER})
    public void createHeroTestValid(String heroName) throws UnknowHeroException {
        Entity hero = new HeroFactory().createHero(heroName);
        assertEquals(30,hero.getHealthPoint());
        assertEquals(0, ((Hero) hero).getArmor());

        switch (hero.getName()){
            case MAGE:
                assertTrue(((Hero) hero).getSpecialAction() instanceof SpecialActionMage);
                break;
            case PALADIN:
                assertTrue(((Hero) hero).getSpecialAction() instanceof SpecialActionPaladin);
                break;
            case GUERRIER:
                assertTrue(((Hero) hero).getSpecialAction() instanceof SpecialActionGuerrier);
                break;
            default:
                break;
        }
    }



    @DisplayName("valid servant")
    @ParameterizedTest
    @MethodSource("validServant")
    public void createCardServantTestValid(String name, int invokeCost, int attack, int hp)
            throws UnknowCardException {
        Servant servant = (Servant) new CardFactory().createCard(name);

        assertEquals(name, servant.getName());
        assertEquals(invokeCost, servant.getInvokeCost());
        assertEquals(hp, servant.getHealthPoint());
        assertEquals(attack, servant.getAttack());
    }

    static Stream<Arguments> validServant() {
        return Stream.of(
                Arguments.of(SANGLIER_BROCHEROC, 1, 1, 1),
                Arguments.of(SOLDAT_DU_COMPTE_DE_L_OR, 1, 1, 2),
                Arguments.of(CHEVAUCHEUR_DE_LOUP, 3, 3, 1),
                Arguments.of(CHEF_DE_RAID, 3, 2, 2),
                Arguments.of(YETI_NOROIT, 4, 4, 5),
                Arguments.of(CHAMPION_FRISSELAME, 4, 3, 2),
                Arguments.of(AVOCAT_COMMIS_D_OFFICE, 2, 0, 7),
                Arguments.of(RECRUE_DE_LA_MAIN_D_ARGENT, 0, 1, 1),
                Arguments.of(IMAGE_MIROIR_SERVANT, 0, 0, 2)
        );
    }

    @Test
    public void createCardException() throws UnknowCardException{
        boolean thrown = false;

        try {
            Entity entity = new CardFactory().createCard("Error");
        } catch (UnknowCardException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    @DisplayName("valid spell")
    @ParameterizedTest
    @MethodSource("validSpell")
    public void createSpellValid(String name, int invokeCost) throws UnknowCardException{
        Spell spell = (Spell) new CardFactory().createCard(name);
        assertEquals(name, spell.getName());
        assertEquals(invokeCost, spell.getUseCost());
    }

    static Stream<Arguments> validSpell() {
        return Stream.of(
                Arguments.of(IMAGE_MIROIR, 1),
                Arguments.of(EXPLOSION_DES_ARCANES, 2),
                Arguments.of(METAMORPHOSE, 4),
                Arguments.of(BENEDICTION_DE_PUISSANCE, 1),
                Arguments.of(CONSECRATION, 4),
                Arguments.of(TOURBILLON, 1),
                Arguments.of(MAITRISE_DU_BLOCAGE, 3)
        );
    }


    @DisplayName("getGeneralCardsValidTest")
    @ParameterizedTest
    @ValueSource(strings = {SANGLIER_BROCHEROC, SOLDAT_DU_COMPTE_DE_L_OR, CHEVAUCHEUR_DE_LOUP, CHEF_DE_RAID, YETI_NOROIT})
    public void getGeneralCardsValidTest(String name){
        List l = CardFactory.getGeneralCards();
        assertTrue(l.contains(name));
    }

    @DisplayName("getGeneralCardsErrorTest")
    @ParameterizedTest
    @ValueSource(strings = {IMAGE_MIROIR, IMAGE_MIROIR_SERVANT, "Error", CHAMPION_FRISSELAME, MAGE})
    public void getGeneralCardsErrorTest(String name){
        List l = CardFactory.getGeneralCards();
        assertFalse(l.contains(name));
    }


    @DisplayName("getGuerrierCardsValidTest")
    @ParameterizedTest
    @ValueSource(strings = {TOURBILLON, AVOCAT_COMMIS_D_OFFICE, MAITRISE_DU_BLOCAGE})
    public void getGuerrierCardsValidTest(String name){
        List l = CardFactory.getGuerrierCards();
        assertTrue(l.contains(name));
    }


    @DisplayName("getGuerrierCardsErrorTest")
    @ParameterizedTest
    @ValueSource(strings = {IMAGE_MIROIR, IMAGE_MIROIR_SERVANT, "Error", CHAMPION_FRISSELAME, MAGE})
    public void getGuerrierCardsErrorTest(String name){
        List l = CardFactory.getGuerrierCards();
        assertFalse(l.contains(name));
    }


    @DisplayName("getMageCardsValidTest")
    @ParameterizedTest
    @ValueSource(strings = {IMAGE_MIROIR, EXPLOSION_DES_ARCANES, METAMORPHOSE})
    public void getMageCardsValidTest(String name){
        List l = CardFactory.getMageCards();
        assertTrue(l.contains(name));
    }


    @DisplayName("getMageCardsErrorTest")
    @ParameterizedTest
    @ValueSource(strings = {IMAGE_MIROIR_SERVANT, "Error", CHAMPION_FRISSELAME, MAGE})
    public void getMageCardsErrorTest(String name){
        List l = CardFactory.getMageCards();
        assertFalse(l.contains(name));
    }


    @DisplayName("getPaladinCardsValidTest")
    @ParameterizedTest
    @ValueSource(strings = {CHAMPION_FRISSELAME, BENEDICTION_DE_PUISSANCE, CONSECRATION,RECRUE_DE_LA_MAIN_D_ARGENT})
    public void getPaladinCardsValidTest(String name){
        List l = CardFactory.getPaladinCards();
        assertTrue(l.contains(name));
    }


    @DisplayName("getPaladinCardsErrorTest")
    @ParameterizedTest
    @ValueSource(strings = {IMAGE_MIROIR_SERVANT, "Error", TOURBILLON, MAGE})
    public void getPaladinCardsErrorTest(String name){
        List l = CardFactory.getPaladinCards();
        assertFalse(l.contains(name));
    }


    @Test
    public void getSpecifiqueCardsValidTest() throws UnknowHeroException{
        assertEquals(CardFactory.getGuerrierCards(), CardFactory.getSpecifiqueCards(GUERRIER));
        assertEquals(CardFactory.getMageCards(), CardFactory.getSpecifiqueCards(MAGE));
        assertEquals(CardFactory.getPaladinCards(), CardFactory.getSpecifiqueCards(PALADIN));
    }


    @Test
    public void getSpecifiqueCardExceptionTest() throws UnknowHeroException{
        boolean thrown = false;

        try {
            CardFactory.getSpecifiqueCards("Error");
        } catch (UnknowHeroException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }
}
