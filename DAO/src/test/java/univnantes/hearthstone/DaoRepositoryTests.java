package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("univnantes.hearthstone")
public class DaoRepositoryTests {


	@Autowired
	EntityRepository entityRepo;

	@Autowired
	GameRepository gameRepo;

	@Autowired
	PlayerRepository playerRepo;

	@Autowired
	EntityRepository entityRepository;

	@Autowired
	Sauvegarde sauvegarde;
	

	@Test
	public void testDeleteGame() {
		List<String> lcards = new ArrayList<String>();
		lcards.add("t");
		lcards.add("t2");
		
		Entity serv = new Servant("ServantName", 1, 0, 0, false, false, null);
		Entity serv2 = new Servant("ServantName2", 1, 0, 0, false, false, null);
		entityRepo.save(serv);
		entityRepo.save(serv2);

		List<Entity> lservants = new ArrayList<Entity>();
		lservants.add(serv);
		
		List<Entity> lservants2 = new ArrayList<Entity>();
		lservants.add(serv2);
		
		Hero hero = new Hero("heroName", 2, 3, null, false);
		Hero hero2 = new Hero("heroName2", 2, 3, null, false);
		entityRepo.save(hero);
		entityRepo.save(hero2);

		
		Player player = new Player("playerName", hero, lcards, lservants, 2, 3, null);
		Player player2 = new Player("playerName2", hero2, lcards, lservants2, 2, 3, null);
		playerRepo.save(player);
		playerRepo.save(player2);
		
		gameRepo.save(new Game(	player,	null, Byte.valueOf("1"), null, null));
		gameRepo.save(new Game(player2, null, Byte.valueOf("1"), null, null));
		
		assertEquals(2, gameRepo.findAll().size());

		Long id = gameRepo.findAll().get(0).getIdGame();
		assertTrue(playerRepo.findAll().contains(player));
		assertTrue(entityRepository.findAll().contains(hero));

		gameRepo.deleteByIdGame(id);

		assertEquals(1, gameRepo.findAll().size());
		assertFalse(playerRepo.findAll().contains(player));
		assertFalse(entityRepository.findAll().contains(hero));
	}

	@Test
	public void TestSaveGame() throws Exception {
		gameRepo.save(new Game(null, null, Byte.valueOf("1"), null, null));
		assertNotNull(gameRepo.findAll());
		assertEquals(1, gameRepo.findAll().get(0).getPlayerTurn());
	}

	@Test
	public void TestSavePlayer() throws Exception {
		playerRepo.save(new Player("test", null, Arrays.asList("t", "t2"), null, 2, 3, null));
		assertNotNull(playerRepo.findAll());
		assertEquals("test", playerRepo.findAll().get(0).getName());
	}

	@Test
	public void TestSaveHero() throws Exception {
		entityRepository.save(new Hero("test", 2, 3, null, false));
		assertNotNull(entityRepository.findAll());
		assertEquals("test", entityRepository.findAll().get(0).getName());
	}

	@Test
	public void TestSaveServant() throws Exception {
		entityRepository.save(new Servant("test", 1, 2, 3, true, true, null));
		assertNotNull(entityRepository.findAll());
		assertEquals("test", entityRepository.findAll().get(0).getName());
	}


	@Test
	public void TestGetHeroFromPlayerFromGame() throws Exception {
		List<String> lcards = new ArrayList<String>();
		lcards.add("t");
		lcards.add("t2");
		
		Entity serv = new Servant("ServantName", 1, 0, 0, false, false, null);
		Entity serv2 = new Servant("ServantName2", 1, 0, 0, false, false, null);
		entityRepo.save(serv);
		entityRepo.save(serv2);

		List<Entity> lservants = new ArrayList<Entity>();
		lservants.add(serv);
		
		List<Entity> lservants2 = new ArrayList<Entity>();
		lservants.add(serv2);
		
		Hero hero = new Hero("heroName", 2, 3, null, false);
		Hero hero2 = new Hero("heroName2", 2, 3, null, false);
		entityRepo.save(hero);
		entityRepo.save(hero2);

		
		Player player = new Player("playerName", hero, lcards, lservants, 2, 3, null);
		Player player2 = new Player("playerName2", hero2, lcards, lservants2, 2, 3, null);
		playerRepo.save(player);
		playerRepo.save(player2);

		gameRepo.save(new Game(	player,	player2, Byte.valueOf("1"), null, null));
		
		assertNotNull(gameRepo.findAll().get(0).getPlayer1());
		assertEquals("heroName", gameRepo.findAll().get(0).getPlayer1().getHero().getName());

	}

	@Test
	public void TestGetServantFromPlayerFromGame() throws Exception {
		List<String> lcards = new ArrayList<String>();
		lcards.add("t");
		lcards.add("t2");
		
		Entity serv = new Servant("ServantName", 1, 0, 0, false, false, null);
		Entity serv2 = new Servant("ServantName2", 1, 0, 0, false, false, null);
		entityRepo.save(serv);
		entityRepo.save(serv2);

		List<Entity> lservants = new ArrayList<Entity>();
		lservants.add(serv);
		
		List<Entity> lservants2 = new ArrayList<Entity>();
		lservants.add(serv2);
		
		Hero hero = new Hero("heroName", 2, 3, null, false);
		Hero hero2 = new Hero("heroName2", 2, 3, null, false);
		entityRepo.save(hero);
		entityRepo.save(hero2);

		
		Player player = new Player("playerName", hero, lcards, lservants, 2, 3, null);
		Player player2 = new Player("playerName2", hero2, lcards, lservants2, 2, 3, null);
		playerRepo.save(player);
		playerRepo.save(player2);
		
		gameRepo.save(new Game(	player,	player2, Byte.valueOf("1"), null, null));
		
		assertNotNull(gameRepo.findAll().get(0).getPlayer1().getPlayingCard());
		assertEquals("ServantName", gameRepo.findAll().get(0).getPlayer1().getPlayingCard().get(0).getName());

	}

	@Test
	public void TestUpdateHeroOfPlayerOfGame() throws Exception {
		
		List<String> lcards = new ArrayList<String>();
		lcards.add("t");
		lcards.add("t2");
		
		Entity serv = new Servant("ServantName", 1, 0, 0, false, false, null);
		entityRepo.save(serv);

		List<Entity> lservants = new ArrayList<Entity>();
		lservants.add(serv);
		
		Hero hero = new Hero("heroName", 2, 3, null, false);
		entityRepo.save(hero);
		
		Player player = new Player("playerName", hero, lcards, lservants, 2, 3, null);
		playerRepo.save(player);
		
		gameRepo.save(new Game(player, null, Byte.valueOf("1"), null, null));
		assertEquals("heroName", gameRepo.findAll().get(0).getPlayer1().getHero().getName());
		Long id = gameRepo.findAll().get(0).getPlayer1().getHero().getIdHero();
		entityRepository.setNameOfIdHero(id, "change");
		assertEquals("change", gameRepo.findAll().get(0).getPlayer1().getHero().getName());
		assertEquals("change", entityRepository.findByIdEntity(id).getName());
	}


	@Test
	public void TestEntity() throws Exception {
		entityRepository.save(new Hero("heroName", 2, 3, null, false));
		entityRepository.save(new Servant("ServantName", 1, 0, 0, false, false, null));
		Hero hero = (Hero) entityRepository.findAll().get(0);
		assertEquals("heroName", hero.getName());
		assertEquals(3, hero.getArmor());

		assertEquals("ServantName", entityRepository.findAll().get(1).getName());

	}
}