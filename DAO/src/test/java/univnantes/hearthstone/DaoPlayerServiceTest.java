package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("univnantes.hearthstone")
public class DaoPlayerServiceTest {
	
	@Autowired
	GameRepository gameRepo;
	
	@Autowired
	PlayerRepository playerRepo;
	
	@Autowired
	EntityRepository entityRepo;
	
	@Autowired
	DaoServicePlayer daoServicePlayer;

	/**
	 * vérifie que l'ajout de la carte en temps qu'entité sur le plateau 
	 * et lasuppression de la carte dans la main du joueur fonctionne
	 * 
	 * on a deja vérifier à ce niveau que la carte existe dans la main du joueur
	 */
	@Test
	public void testInvokeCard() {
		Hero h = new Hero("heroName", 2,3,null, false);
		entityRepo.save(h);
		
		Entity e = new Servant("serv",1, 0, 0, false, false, null);
		entityRepo.save(e);
		
		List<String> hand = new ArrayList<>();
		hand.add("ServantName");
		hand.add("ServantName");

		List<Entity> playing = new ArrayList<>();
		playing.add(e);
		
		Player player = new Player("playerName", h, hand, playing, 2,3, null);
		playerRepo.save(player);
		
		//enregistrement de la partie
		gameRepo.save(new Game(player, null, Byte.valueOf("1"), null, null));

		assertEquals(2, playerRepo.findAll().get(0).getHand().size());
		assertEquals(1, playerRepo.findAll().get(0).getPlayingCard().size());

		daoServicePlayer.invokeCard(player, new Servant("ServantName",1, 0, 0, false, false, null));
		
		//vérification de l'ajout du servant dans les carte jouée
		assertEquals(3, entityRepo.findAll().size());
		assertEquals(2, playerRepo.findAll().get(0).getPlayingCard().size());
		assertEquals("ServantName", playerRepo.findAll().get(0).getPlayingCard().get(1).getName());
		
		//vérification carte supprimer dans la main du joueur
		assertEquals(1, playerRepo.findAll().get(0).getHand().size());

		//verification que le joueur n'est pas dupliquer dans la base
		assertEquals(1,playerRepo.findAll().size());
		assertEquals("playerName",playerRepo.findAll().get(0).getName());
	}
		

}