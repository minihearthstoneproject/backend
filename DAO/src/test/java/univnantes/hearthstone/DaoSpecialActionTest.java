package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static univnantes.hearthstone.modele.factories.CardFactory.IMAGE_MIROIR_SERVANT;
import static univnantes.hearthstone.modele.factories.CardFactory.RECRUE_DE_LA_MAIN_D_ARGENT;
import static univnantes.hearthstone.modele.factories.HeroFactory.MAGE;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.Spell;
import univnantes.hearthstone.modele.specialAction.*;
import univnantes.hearthstone.modele.specialAction.SpecialActionChefDeRaidInvoke;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("univnantes.hearthstone")
public class DaoSpecialActionTest {

    @Autowired
    EntityRepository entityRepo;

    @Autowired
    GameRepository gameRepo;

    @Autowired
    PlayerRepository playerRepo;

    @Autowired
    Sauvegarde sauvegardeRepo;

    Game game;
    Hero hero;
    Servant servant1;
    Servant servant2;
    Hero heroAdv;
    Servant servantAdv1;
    Servant servantAdv2;
    List<SpecialAction> l;

    @Before
    public void Initialisation(){
        hero = new Hero(MAGE, 30, 0, null, false);
        servant1 = new Servant("servantName", 1, 10, 0, false, false, null);
        servant2 = new Servant("servantName1", 1, 10, 0, false, false, null);
        heroAdv =  new Hero(MAGE, 30, 0, null, false);
        servantAdv1 = new Servant("servantNameAdv1", 1, 10, 0, false, false, null);
        servantAdv2 = new Servant("servantNameAdv2", 1, 10, 0, false, false, null);

        entityRepo.save(hero);
        entityRepo.save(servant1);
        entityRepo.save(servant2);
        entityRepo.save(heroAdv);
        entityRepo.save(servantAdv1);
        entityRepo.save(servantAdv2);

        List<String> hand = new ArrayList<>();
        hand.add("ServantName");
        hand.add("ServantName");

        List<Entity> playingCardP1 = new ArrayList<>();
        playingCardP1.add(servant1);
        playingCardP1.add(servant2);

        List<Entity> playingCardP2 = new ArrayList<>();
        playingCardP2.add(servantAdv1);
        playingCardP2.add(servantAdv2);

        Player player1 = new Player("playerName1", hero, hand, playingCardP1, 2, 3, null);
        Player player2 = new Player("playerName2", heroAdv, hand, playingCardP2, 2, 3, null);
        playerRepo.save(player1);
        playerRepo.save(player2);
        
        // enregistrement de la partie
        gameRepo.save(new Game(player1, player2, Byte.valueOf("1"), null, null));

        game = gameRepo.findAll().get(0);

        l = new ArrayList<SpecialAction>();

    }


    @Test
    public void TestPaladinJ1() throws UnknowCardException, UnknowHeroException{
        ((Hero) entityRepo.findAll().get(0)).setSpecialAction(new SpecialActionPaladin());

        assertEquals(6, entityRepo.findAll().size());
        assertEquals(2, game.getPlayer1().getPlayingCard().size());

        ((Hero) entityRepo.findAll().get(0)).getSpecialAction().execute(sauvegardeRepo, game, hero, null);

        assertEquals(3, game.getPlayer1().getPlayingCard().size());
        assertEquals(7, entityRepo.findAll().size());
        assertEquals(RECRUE_DE_LA_MAIN_D_ARGENT, entityRepo.findAll().get(6).getName());
    }

    @Test
    public void TestPaladinJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);
        ((Hero) entityRepo.findByIdEntity(heroAdv.getIdHero())).setSpecialAction(new SpecialActionPaladin());

        assertEquals(6, entityRepo.findAll().size());
        assertEquals(2, game.getPlayer2().getPlayingCard().size());

        ((Hero) entityRepo.findByIdEntity(heroAdv.getIdHero())).getSpecialAction().execute(sauvegardeRepo, game, heroAdv, null);

        assertEquals(3, game.getPlayer2().getPlayingCard().size());
        assertEquals(7, entityRepo.findAll().size());
        assertEquals(RECRUE_DE_LA_MAIN_D_ARGENT, entityRepo.findAll().get(6).getName());
    }


    @Test
    public void TestGuerrier() throws UnknowCardException, UnknowHeroException{
        ((Hero) entityRepo.findAll().get(0)).setSpecialAction(new SpecialActionGuerrier());

        assertEquals(0, ((Hero)  entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        ((Hero) entityRepo.findAll().get(0)).getSpecialAction().execute(sauvegardeRepo, game, hero, null);
        assertEquals(2, ((Hero)  entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());

    }

    @Test
    public void TestMage() throws UnknowCardException, UnknowHeroException{

        //Inflige 1 point de degat a une entité ennemie
        ((Hero) entityRepo.findAll().get(0)).setSpecialAction(new SpecialActionMage());

        //case on servant
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        ((Hero) entityRepo.findAll().get(0)).getSpecialAction().execute(sauvegardeRepo, game, null, servantAdv1);
        assertEquals(9,  entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());

        //case on hero
        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        ((Hero) entityRepo.findAll().get(0)).getSpecialAction().execute(sauvegardeRepo, game, null, heroAdv);
        assertEquals(29,  entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0,  ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());

    }
    
    @Test
    public void TestCharge() throws UnknowCardException, UnknowHeroException{
    	
    	List<SpecialAction> l = new ArrayList<SpecialAction>();
		l.add(new SpecialActionCharge());
        servant1.setSpecialAction(l);
        entityRepo.save(servant1);
        
        assertFalse(entityRepo.findByIdEntity(servant1.getIdEntity()).isCanAttack());
        //Permet d'attaquer dès le premier tour
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);
        assertTrue(entityRepo.findByIdEntity(servant1.getIdEntity()).isCanAttack());
    }

    @Test
    public void TestChefDeRaidInvokeJ1() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionChefDeRaidInvoke());
        servant1.setSpecialAction(l);
        entityRepo.save(servant1);

        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());

        //Ajoute 1 point d'attaque à tous les serviteurs du même camps
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(1 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(1 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());
    }

    @Test
    public void TestChefDeRaidInvokeJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionChefDeRaidInvoke());
        servantAdv1.setSpecialAction(l);
        entityRepo.save(servantAdv1);

        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());

        //Ajoute 1 point d'attaque à tous les serviteurs du même camps
        ((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(1 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(1 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());
    }


    @Test
    public void TestChefDeRaidDeadJ1() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionChefDeRaidDead());
        servant1.setSpecialAction(l);
        servantAdv1.setAttack(5);
        servantAdv2.setAttack(5);
        entityRepo.save(servant1);
        entityRepo.save(servant2);

        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(5 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(5 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());

        //Ajoute 1 point d'attaque à tous les serviteurs du même camps
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(4 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(4 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());
    }

    @Test
    public void TestChefDeRaidDeadJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionChefDeRaidDead());
        servantAdv1.setSpecialAction(l);
        servant1.setAttack(5);
        servant2.setAttack(5);
        entityRepo.save(servantAdv1);
        entityRepo.save(servantAdv2);

        assertEquals(5 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(5 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());

        //Ajoute 1 point d'attaque à tous les serviteurs du même camps
        ((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(4 ,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(4 ,((Servant) entityRepo.findByIdEntity(servant2.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(0 ,((Servant) entityRepo.findByIdEntity(servantAdv2.getIdEntity())).getAttack());
    }

    @Test
    public void TestProvocation() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionProvocation());
        servant1.setSpecialAction(l);
        entityRepo.save(servant1);

        assertFalse(((Servant) entityRepo.findAll().get(1)).isProvocation());
        //Ajoute provocation à un servant
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);
        assertTrue(((Servant) entityRepo.findAll().get(1)).isProvocation());

    }


    @Test
    public void TestConsecrationJ1() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionConsecration());
        servant1.setSpecialAction(l);
        entityRepo.save(servant1);
        heroAdv.addArmor(1);
        entityRepo.save(heroAdv);


        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(1, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());

        //Inflige 2 points de dégâts à tous les adversaires
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

        assertEquals(29, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(8, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(8, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());
    }


    @Test
    public void TestConsecrationJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionConsecration());
        servantAdv1.setSpecialAction(l);
        entityRepo.save(servantAdv1);
        hero.addArmor(1);
        entityRepo.save(hero);


        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(1, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());


        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());

        //Inflige 2 points de dégâts à tous les adversaires
        ((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servantAdv1, null);

        assertEquals(29, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(8, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(8, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());


        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());
    }


    @Test
    public void TestExplosionDesArcanesJ1() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionExplosionArcanes());
        servant1.setSpecialAction(l);
        entityRepo.save(servant1);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant2.getIdEntity()).getHealthPoint());

        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());

        //Inflige 1 point de dégât à tous les serviteurs adverses
        ((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, null);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant2.getIdEntity()).getHealthPoint());

        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(9, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(9, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());
    }


    @Test
    public void TestExplosionDesArcanesJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionExplosionArcanes());
        servantAdv1.setSpecialAction(l);
        entityRepo.save(servantAdv1);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());

        //Inflige 1 point de dégât à tous les serviteurs adverses
        ((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getSpecialAction().get(0).execute(sauvegardeRepo, game, servantAdv1, null);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdEntity())).getArmor());
        assertEquals(9, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(9, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdEntity())).getArmor());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());
    }

    @Test
    public void TestImageMirroirJ1() throws UnknowCardException, UnknowHeroException{
        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionImageMiroir());

        assertEquals(2, game.getPlayer1().getPlayingCard().size());
        //Ajout de deux serviteurs avec provocation et 0/2
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, null);

        assertEquals(4, game.getPlayer1().getPlayingCard().size());

        assertEquals(IMAGE_MIROIR_SERVANT, game.getPlayer1().getPlayingCard().get(2).getName());
        assertEquals(IMAGE_MIROIR_SERVANT, game.getPlayer1().getPlayingCard().get(3).getName());

        assertTrue(((Servant) game.getPlayer1().getPlayingCard().get(2)).isProvocation());
        assertTrue(((Servant) game.getPlayer1().getPlayingCard().get(3)).isProvocation());
        assertEquals(2, game.getPlayer1().getPlayingCard().get(2).getHealthPoint());
        assertEquals(2, game.getPlayer1().getPlayingCard().get(3).getHealthPoint());
        assertEquals(0,((Servant) game.getPlayer1().getPlayingCard().get(2)).getAttack());
        assertEquals(0,((Servant) game.getPlayer1().getPlayingCard().get(3)).getAttack());
    }

    @Test
    public void TestImageMirroirJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionImageMiroir());

        assertEquals(2, game.getPlayer1().getPlayingCard().size());
        //Ajout de deux serviteurs avec provocation et 0/2
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, null);

        assertEquals(4, game.getPlayer2().getPlayingCard().size());

        assertEquals(IMAGE_MIROIR_SERVANT, game.getPlayer2().getPlayingCard().get(2).getName());
        assertEquals(IMAGE_MIROIR_SERVANT, game.getPlayer2().getPlayingCard().get(3).getName());

        assertTrue(((Servant) game.getPlayer2().getPlayingCard().get(2)).isProvocation());
        assertTrue(((Servant) game.getPlayer2().getPlayingCard().get(3)).isProvocation());
        assertEquals(2, game.getPlayer2().getPlayingCard().get(2).getHealthPoint());
        assertEquals(2, game.getPlayer2().getPlayingCard().get(3).getHealthPoint());
        assertEquals(0,((Servant) game.getPlayer2().getPlayingCard().get(2)).getAttack());
        assertEquals(0,((Servant) game.getPlayer2().getPlayingCard().get(3)).getAttack());
    }


    @Test
    public void TestMaitriseDuBlocageJ1() throws UnknowCardException, UnknowHeroException {
        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionMaitriseDuBlocage());

        assertEquals(0,((Hero) entityRepo.findByIdEntity(hero.getIdHero())).getArmor());
        assertEquals(2, game.getPlayer1().getHand().size());

        // Ajoute 5 d'armure et pioche
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, null);

        assertEquals(5,((Hero) entityRepo.findByIdEntity(hero.getIdHero())).getArmor());
        assertEquals(4, game.getPlayer1().getHand().size());
    }

    @Test
    public void TestMaitriseDuBlocageJ2() throws UnknowCardException, UnknowHeroException {
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionMaitriseDuBlocage());

        assertEquals(0,((Hero) entityRepo.findByIdEntity(hero.getIdHero())).getArmor());
        assertEquals(2, game.getPlayer1().getHand().size());

        // Ajoute 5 d'armure et pioche
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, null);

        assertEquals(5,((Hero) entityRepo.findByIdEntity(heroAdv.getIdHero())).getArmor());
        assertEquals(4, game.getPlayer2().getHand().size());
    }


    @Test
    public void TestTourbillon() throws UnknowCardException, UnknowHeroException{
        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionTourbillon());

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdHero()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdHero())).getArmor());
        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdHero()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdHero())).getArmor());

        assertEquals(10, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servant2.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());

        //Enleve 1 point de vie à tous les serviteurs présents sur le plateau
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, null);

        assertEquals(30, entityRepo.findByIdEntity(hero.getIdHero()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(hero.getIdHero())).getArmor());
        assertEquals(30, entityRepo.findByIdEntity(heroAdv.getIdHero()).getHealthPoint());
        assertEquals(0, ((Hero) entityRepo.findByIdEntity(heroAdv.getIdHero())).getArmor());

        assertEquals(9, entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());
        assertEquals(9, entityRepo.findByIdEntity(servant2.getIdEntity()).getHealthPoint());
        assertEquals(9, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(9, entityRepo.findByIdEntity(servantAdv2.getIdEntity()).getHealthPoint());
    }

    @Test
    public void TestBenedictionDePuissance() throws UnknowCardException, UnknowHeroException{
        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionBenedictionDePuissance());

        assertEquals(0,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());

        //Ajout de 3 points d'attaque à 1 serviteur
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, servant1);

        assertEquals(3,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());

    }

    @Test
    public void TestMetamorphose() throws UnknowCardException, UnknowHeroException{
        Spell spell = new Spell();
        spell.setSpecialAction(new SpecialActionMetamorphose());

        assertEquals(0,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(10, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());

        //Transforme un serviteur adverse en serviteur 1/1
        spell.getSpecialAction().execute(sauvegardeRepo, game, null, servantAdv1);

        assertEquals(1,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(1, entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
        assertEquals(null, ((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getSpecialAction());
    }

    @Test
    public void TestVolDeVieJ1() throws UnknowCardException, UnknowHeroException{
        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionVolDeVie());
        servant1.setSpecialAction(l);
        servant1.setAttack(5);
        entityRepo.save(servant1);


        assertEquals(30,  entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(5,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(10,  entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());

        //Lorsque le serviteur inflige des dégâts, votre hero récupère le même nombre de point de vie
        servant1.getSpecialAction().get(0).execute(sauvegardeRepo, game, servant1, servantAdv1);

        assertEquals(35,  entityRepo.findByIdEntity(hero.getIdEntity()).getHealthPoint());
        assertEquals(5,((Servant) entityRepo.findByIdEntity(servant1.getIdEntity())).getAttack());
        assertEquals(5,  entityRepo.findByIdEntity(servantAdv1.getIdEntity()).getHealthPoint());
    }

    @Test
    public void TestVolDeVieJ2() throws UnknowCardException, UnknowHeroException{
        game.setPlayerTurn((byte)2);
        gameRepo.save(game);

        List<SpecialAction> l = new ArrayList<SpecialAction>();
        l.add(new SpecialActionVolDeVie());
        servantAdv1.setSpecialAction(l);
        servantAdv1.setAttack(5);
        entityRepo.save(servantAdv1);


        assertEquals(30,  entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(5,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(10,  entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

        //Lorsque le serviteur inflige des dégâts, votre hero récupère le même nombre de point de vie
        servantAdv1.getSpecialAction().get(0).execute(sauvegardeRepo, game, servantAdv1, servant1);

        assertEquals(35,  entityRepo.findByIdEntity(heroAdv.getIdEntity()).getHealthPoint());
        assertEquals(5,((Servant) entityRepo.findByIdEntity(servantAdv1.getIdEntity())).getAttack());
        assertEquals(5,  entityRepo.findByIdEntity(servant1.getIdEntity()).getHealthPoint());

    }
}
