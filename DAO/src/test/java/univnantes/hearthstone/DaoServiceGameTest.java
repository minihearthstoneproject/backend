package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.repository.GameRepository;
import univnantes.hearthstone.dao.repository.PlayerRepository;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;

@RunWith(SpringRunner.class)
@DataJpaTest
@ComponentScan("univnantes.hearthstone")
public class DaoServiceGameTest {
	
	@Autowired
	EntityRepository entityRepo;

	@Autowired
	GameRepository gameRepo;

	@Autowired
	PlayerRepository playerRepo;

	@Autowired
	EntityRepository entityRepository;
	
	@Autowired
	IDaoServiceGame daoServiceGame;
	
	@Autowired
	IDaoServicePlayer daoServicePlayer;
	
	@Test
	public void testDeleteGame() {
		List<String> lcards = new ArrayList<String>();
		lcards.add("t");
		lcards.add("t2");
		
		Entity serv = new Servant("ServantName", 1, 0, 0, false, false, null);
		Entity serv2 = new Servant("ServantName2", 1, 0, 0, false, false, null);
		entityRepo.save(serv);
		entityRepo.save(serv2);

		List<Entity> lservants = new ArrayList<Entity>();
		lservants.add(serv);
		
		List<Entity> lservants2 = new ArrayList<Entity>();
		lservants.add(serv2);
		
		Hero hero = new Hero("heroName", 2, 3, null, false);
		Hero hero2 = new Hero("heroName2", 2, 3, null, false);
		entityRepo.save(hero);
		entityRepo.save(hero2);

		
		Player player = new Player("playerName", hero, lcards, lservants, 2, 3, null);
		Player player2 = new Player("playerName2", hero2, lcards, lservants2, 2, 3, null);
		playerRepo.save(player);
		playerRepo.save(player2);
		
		gameRepo.save(new Game(	player,	null, Byte.valueOf("1"), null, null));
		gameRepo.save(new Game(player2, null, Byte.valueOf("1"), null, null));
		
		assertEquals(2, gameRepo.findAll().size());

		Game gameTest = gameRepo.findAll().get(0);
		
		assertTrue(playerRepo.findAll().contains(player));
		assertTrue(entityRepository.findAll().contains(hero));

		daoServiceGame.deleteGame(gameTest);

		assertEquals(1, gameRepo.findAll().size());
		assertFalse(playerRepo.findAll().contains(player));
		assertFalse(entityRepository.findAll().contains(hero));
	}
	
	
	@Test
	public void testCreateGame() {
		
		Player p = daoServicePlayer.createPlayer("testName", "key");
		Game g = daoServiceGame.createGameWithPlayer("gameKey", p);

		assertEquals("testName", daoServiceGame.get(g.getIdGame()).getPlayer1().getName());
	}
}
