# Projet Mini-Hearthstone
## Conception préliminaire

* Maxence COUTAND 
* Nicolas VANNIER
* Tom LE BERRE

#### I - Division en plusieurs composants


* [DAO](https://gitlab.com/max_c/hearthstone/tree/master/DAO)

Le composant **DAO** permettant la gestion des données. Le composants back-end passe par ce composant pour effectuer des requêtes vers l'espace de stockage 
L'utilisation de DAO permet de s'abstraire de la façon dont les données sont stockées au niveau des objets métiers. Ainsi, le changement du mode de stockage ne remet pas en cause
le reste de l'application. En effet, seules ces classes dites "techniques" seront à modifier (et donc à re-tester). Cette souplesse implique cependant un coût additionnel, 
dû à une plus grande complexité de mise en œuvre.

* [HearthstoneWebApp](https://gitlab.com/max_c/hearthstone/tree/master/HearthstoneWebApp)

Le composant **HearthstoneWebApp** permet la gestion des différentes parties (synchroniser les 	joueurs avec leur partie, déroulement de la partie). 


* [HearthstoneAngularClient]()

Le composant **HearthstoneAngularClient** est la partie Front-end du projet qui a été créer grâce au framework angular, il s'agit finalement des éléments du site que l'on voit à l'écran et avec lesquels on peut interagir.
Ces éléments sont composés de HTML, CSS/SCSS et de Typescript/Javascript contrôlés par le navigateur web de l'utilisateur. Il permet également de communiquer vers les composants back-end.


## Diagramme DAO

![Diagramme DAO](https://gitlab.com/minihearthstoneproject/backend/raw/master/Report/diagrams/diagramDAO.png)


## Diagramme modele

![Diagramme model](https://gitlab.com/minihearthstoneproject/backend/raw/master/Report/diagrams/diagramModel.png)

## Diagramme Special Action

![Diagramme model](https://gitlab.com/minihearthstoneproject/backend/raw/master/Report/diagrams/diagramSpecialAction.png)
