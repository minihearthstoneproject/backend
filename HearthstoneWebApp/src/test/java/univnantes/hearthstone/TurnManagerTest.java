package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceEntity;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.util.TurnManager;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
@ComponentScan("univnantes.hearthstone")
public class TurnManagerTest {

	@InjectMocks
	@Autowired
	TurnManager turnManager;

	@InjectMocks
	@Autowired
	PiocheManager piocheManager;
	@Mock
	protected DaoServiceGame daoServiceGame;

	@Mock
	protected DaoServiceEntity daoServiceEntity;

	@Mock
	protected DaoServicePlayer daoServicePlayer;

	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;

	@Autowired
	@InjectMocks
	HeroFactory heroFactory;

	@Autowired
	@InjectMocks
	CardFactory cardFactory;

	@Mock
	Sauvegarde sauvegarde;

	Hero heroPlayer1;
	Hero heroPlayer2;

	protected Player player1;
	protected Player player2;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		player1 = new Player("user1", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key");
		player2 = new Player("user2", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key2");
	}
	/**
	 * verifie que letour a bien été changer lors du time out
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testTimeOut() throws InterruptedException {
		Game game = new Game(player1, null, (byte) 1, null, null);
		assertEquals((byte) 1, game.getPlayerTurn());
		when(daoServiceGame.turnTimeOut(game.getIdGame())).thenReturn(changeTurnGame(game, (byte) 2));
		turnManager.activeTimerTest(game.getIdGame(), 1000);
		Thread.sleep(3000);
		assertEquals((byte) 2, game.getPlayerTurn());
	}

	@Test
	public void testInitGame() throws UnknowHeroException {
		Hero heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		Hero heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);
		player1.setHero(heroPlayer1);
		player2.setHero(heroPlayer2);
		Game game = new Game(player1, player2, (byte) 1, null, null);
		
		when(daoServicePlayer.addHandCard(eq(player1), anyString())).thenReturn(playeraddhandcard(player1));
		when(daoServicePlayer.addHandCard(eq(player2), anyString())).thenReturn(playeraddhandcard(player2));
		
		player1.getHand().clear();
		player2.getHand().clear();

		assertEquals(0, player1.getHand().size());
		assertEquals(0, player2.getHand().size());
		
		turnManager.initGame(game);
		assertEquals(3, player1.getHand().size());
		assertEquals(3, player2.getHand().size());
	}
	
	@Test
	public void testInitTurn() throws UnknowHeroException, UnknowCardException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);

		Entity serv2 = (Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR);
		serv2.setCanAttack(true);
		serv2.setIdEntity(Long.valueOf(4));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add(serv2);

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(5);
		int maxmana = 6;
		player1.setMaxMana(maxmana);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, (byte) 1, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServicePlayer.addHandCard(eq(player1), anyString())).thenReturn(playeraddhandcard(player1));
		
		turnManager.initTurn(game.getIdGame());
		
		verify(daoServiceEntity, times(1)).canAttackTrue(serv1);
		verify(daoServiceEntity, times(1)).canAttackTrue(heroPlayer1);
		verify(daoServicePlayer, times(1)).save(player1);
		verify(daoServiceGame, times(1)).saveGame(game);
		assertEquals(maxmana+1, player1.getMana());
	}

	protected Game changeTurnGame(Game game, byte turn) {
		game.setPlayerTurn(turn);
		return game;
	}
	
	
	protected Player playeraddhandcard(Player player) {
		 player.addHandCard("string");
		return player;
	}

}
