package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceEntity;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.DontHaveTheCardException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
@ComponentScan("univnantes.hearthstone")
public class ActionTest {

	@Mock
	protected DaoServiceGame daoServiceGame;

	@Mock
	protected DaoServiceEntity daoServiceEntity;

	@Mock
	protected DaoServicePlayer daoServicePlayer;

	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;

	@Autowired
	@InjectMocks
	HeroFactory heroFactory;

	@Autowired
	@InjectMocks
	CardFactory cardFactory;

	@Mock
	Sauvegarde sauvegarde;

	Hero heroPlayer1;
	Hero heroPlayer2;

	protected Player player1;
	protected Player player2;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		player1 = new Player("user1", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key");
		player2 = new Player("user2", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key2");
	}

	@Test
	public void TestHeroPlayer1ToHeroPlayer2() throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add((Servant) cardFactory.createCard(CardFactory.YETI_NOROIT));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add((Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR));

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(false);

		gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(), player1.getHero().getIdEntity(),
				player2.getHero().getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(heroPlayer1);
		verify(daoServiceEntity, times(1)).isDead(heroPlayer2.getIdEntity());
		verify(daoServicePlayer).StealManaPlayer(player1, 2);
		assertEquals(10 - 1, heroPlayer2.getHealPoint());
		assertFalse(heroPlayer1.isCanAttack());
	}

	@Test
	public void TestHeroPlayer1ToHeroPlayer2AndKill()
			throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(1);

		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add((Servant) cardFactory.createCard(CardFactory.YETI_NOROIT));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add((Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR));

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(true);

		gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(), player1.getHero().getIdEntity(),
				player2.getHero().getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(heroPlayer1);
		verify(daoServiceEntity, times(1)).isDead(heroPlayer2.getIdEntity());
		verify(daoServiceGame, times(1)).deleteGame(game);
		verify(daoServicePlayer).StealManaPlayer(player1, 2);
		assertEquals(0, heroPlayer2.getHealPoint());
		assertFalse(heroPlayer1.isCanAttack());
	}

	@Test
	public void TestHeroPlayer1ToServantPlayer2() throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);

		Entity serv2 = (Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR);
		serv2.setCanAttack(true);
		serv2.setIdEntity(Long.valueOf(4));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add(serv2);

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(true);

		Game gg = gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(), player1.getHero().getIdEntity(),
				player2.getPlayingCard().get(0).getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(heroPlayer1);
		verify(daoServiceEntity, times(1)).isDead(player2.getPlayingCard().get(0).getIdEntity());
		verify(daoServicePlayer).StealManaPlayer(player1, 2);
		assertEquals(1, player2.getPlayingCard().get(0).getHealthPoint());
		assertFalse(heroPlayer1.isCanAttack());
	}

	@Test
	public void TestServantPlayer1ToHeroPlayer2() throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add((Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR));

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(false);

		gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(),
				player1.getPlayingCard().get(0).getIdEntity(), player2.getHero().getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(heroPlayer2);
		verify(sauvegarde, times(1)).save(player1.getPlayingCard().get(0));
		verify(daoServiceEntity, times(1)).isDead(heroPlayer2.getIdEntity());
		assertEquals(10 - 4, heroPlayer2.getHealPoint());
		assertFalse(player1.getPlayingCard().get(0).isCanAttack());
	}

	@Test
	public void TestServantPlayer1ToHeroPlayer2AndKill()
			throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(4);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add((Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR));

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(true);

		gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(),
				player1.getPlayingCard().get(0).getIdEntity(), player2.getHero().getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(heroPlayer2);
		verify(sauvegarde, times(1)).save(player1.getPlayingCard().get(0));
		verify(daoServiceEntity, times(1)).isDead(heroPlayer2.getIdEntity());
		verify(daoServiceGame, times(1)).deleteGame(game);
		assertEquals(0, heroPlayer2.getHealPoint());
		assertFalse(player1.getPlayingCard().get(0).isCanAttack());
	}

	@Test
	public void TestServantPlayer1ToServantPlayer2AndKill()
			throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);

		Entity serv2 = (Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR);
		serv2.setCanAttack(true);
		serv2.setIdEntity(Long.valueOf(4));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add(serv2);

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(player2.getPlayingCard().get(0).getIdEntity())).thenReturn(true);

		gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(),
				player1.getPlayingCard().get(0).getIdEntity(), player2.getPlayingCard().get(0).getIdEntity());

		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(sauvegarde, times(1)).save(player2.getPlayingCard().get(0));
		verify(sauvegarde, times(1)).save(player1.getPlayingCard().get(0));
		verify(daoServiceEntity, times(1)).isDead(player2.getPlayingCard().get(0).getIdEntity());
		verify(daoServicePlayer, times(1)).deleteServant(player2, (Servant) player2.getPlayingCard().get(0));
		assertEquals(-2, player2.getPlayingCard().get(0).getHealthPoint());
		assertFalse(player1.getPlayingCard().get(0).isCanAttack());
	}

	@Test
	public void TestServantPlayer1ToHeroPlayer2AndProvocation()
			throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setCanAttack(true);
		serv1.setIdEntity(Long.valueOf(3));
		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add(serv1);

		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		Entity serv2 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv2.setCanAttack(true);
		((Servant) serv2).setProvocation(true);
		serv2.setIdEntity(Long.valueOf(4));
		listEntityPlayer2.add(serv2);

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);

		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceEntity.isDead(heroPlayer2.getIdEntity())).thenReturn(false);


		try {
			
			gameControllerImpl.action(player1.getIdPlayer(), game.getIdGame(),
					player1.getPlayingCard().get(0).getIdEntity(), player2.getHero().getIdEntity());	
			
			assert false;
		} catch (Exception e) {
			assert (e instanceof IllegalArgumentException);
		}


	}
}
