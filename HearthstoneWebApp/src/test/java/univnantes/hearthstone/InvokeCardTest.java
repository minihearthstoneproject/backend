package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceEntity;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.DontHaveTheCardException;
import univnantes.hearthstone.exception.EnoughManaException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
@ComponentScan("univnantes.hearthstone")
public class InvokeCardTest  {

	@Mock
	protected DaoServiceGame daoServiceGame;

	@Mock
	protected DaoServiceEntity daoServiceEntity;

	@Mock
	protected DaoServicePlayer daoServicePlayer;

	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;

	@Autowired
	@InjectMocks
	HeroFactory heroFactory;

	@Autowired
	@InjectMocks
	CardFactory cardFactory;

	@Mock
	Sauvegarde sauvegarde;

	Hero heroPlayer1;
	Hero heroPlayer2;

	protected Player player1;
	protected Player player2;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		player1 = new Player("user1", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key");
		player2 = new Player("user2", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key2");
	}

	/**
	 * test le seul cas ou le joueur n'a pas la carte dans sa main
	 * 
	 * @throws EnoughManaException
	 * @throws UnknowCardException
	 * @throws CantPlayException
	 * @throws DontHaveTheCardException
	 * @throws UnknowHeroException 
	 */
	@Test
	public void testInokeCard_HaveNotCardInHisHand()
			throws DontHaveTheCardException, CantPlayException, UnknowCardException, EnoughManaException, UnknowHeroException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(4);

		player1.setHand(new ArrayList<String>());
		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);


		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);

		try {
			gameControllerImpl.invokeCard(game.getPlayer1().getIdPlayer(), game.getIdGame(),
					CardFactory.YETI_NOROIT, null);
			assert false;
		} catch (Exception e) {
			assert (e instanceof DontHaveTheCardException);
		}
	}

	/**
	 * test le seul cas ou le joueur n'a pas suffisement de mana
	 * 
	 * @throws EnoughManaException
	 * @throws UnknowCardException
	 * @throws CantPlayException
	 * @throws DontHaveTheCardException
	 * @throws UnknowHeroException 
	 */
	@Test
	public void testInokeCard_EnoughMana()
			throws DontHaveTheCardException, CantPlayException, UnknowCardException, EnoughManaException, UnknowHeroException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(4);


		ArrayList<String> listhandplayer1 = new ArrayList<String>();
		listhandplayer1.add(CardFactory.YETI_NOROIT);
		player1.setHand(listhandplayer1);
		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(-1);


		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);

		try {
			gameControllerImpl.invokeCard(game.getPlayer1().getIdPlayer(), game.getIdGame(),
					CardFactory.YETI_NOROIT, null);
			assert false;
		} catch (Exception e) {
			assert (e instanceof EnoughManaException);
		}
	}



	@Test
	public void testInvokeCardServant() throws UnknowHeroException, UnknowCardException, DontHaveTheCardException,
			CantPlayException, EnoughManaException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(4);

		Entity yeti = cardFactory.createCard(CardFactory.YETI_NOROIT);
		
		ArrayList<String> listhandplayer1 = new ArrayList<String>();
		listhandplayer1.add(CardFactory.YETI_NOROIT);
		player1.setHand(listhandplayer1);
		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);


		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		
		gameControllerImpl.invokeCard(player1.getIdPlayer(), game.getIdGame(), CardFactory.YETI_NOROIT, null);
	
		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(daoServicePlayer).StealManaPlayer(player1, 4);
		verify(daoServicePlayer).invokeCard(eq(player1), any());
	}
	
	@Test
	public void testInvokeSpell() throws UnknowHeroException, UnknowCardException, DontHaveTheCardException, CantPlayException, EnoughManaException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		Entity serv1 = (Servant) cardFactory.createCard(CardFactory.YETI_NOROIT);
		serv1.setIdEntity(Long.valueOf(3));
		listEntityPlayer1.add(serv1);
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		Entity serv2 = (Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR);
		serv2.setIdEntity(Long.valueOf(4));
		listEntityPlayer2.add(serv2);

		ArrayList<String> listHandPlayer1 = new ArrayList<String>();
		listHandPlayer1.add(CardFactory.BENEDICTION_DE_PUISSANCE);
		
 		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setHand(listHandPlayer1);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);
		
		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		
		gameControllerImpl.invokeCard(player1.getIdPlayer(), game.getIdGame(), CardFactory.BENEDICTION_DE_PUISSANCE, player2.getPlayingCard().get(0).getIdEntity());
	
		verify(daoServiceGame, times(2)).get(game.getIdGame());
		verify(daoServicePlayer).StealManaPlayer(player1, 1);
		verify(daoServicePlayer).removeHandCard(eq(player1), any());
		verify(daoServiceEntity, times(1)).isDead(player2.getPlayingCard().get(0).getIdEntity());

	}
}
