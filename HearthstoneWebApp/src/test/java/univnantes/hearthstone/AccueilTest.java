package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.AccueilControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.exception.AlreadyHaveHeroException;
import univnantes.hearthstone.exception.GameAlreadyFullException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.util.TurnManager;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
public class AccueilTest extends UtilsTest{
	
	
	@Mock
	TurnManager turnManager;
	
	@Mock
	PiocheManager piocheManager;
	
	@Autowired
	@InjectMocks
	AccueilControllerImpl AccueilControllerImpl;
	
	@Autowired
	@InjectMocks
	HeroFactory heroFactory;

	
	@Test
	public void testCreateGame() {

		assertEquals(NAME_J1, AccueilControllerImpl.createGame("gameKey", player1.getName(), "userKey").getPlayer1().getName());
	}

	@Test
	public void testJoinGameNotComplet() throws GameAlreadyFullException {
		when(daoServiceGame.isFull(gameWithPlayer1.getIdGame()))
		.thenReturn(false);
		Game game = AccueilControllerImpl.joinGame(gameWithPlayer1.getIdGame(), player2.getName(), "userKey");
		assertNotNull(game.getPlayer1());
		assertNotNull(game.getPlayer2());
		assertEquals(NAME_J1, game.getPlayer1().getName());
		assertEquals(NAME_J2, game.getPlayer2().getName());
	}

	@Test
	public void testJoinGameComplet() throws GameAlreadyFullException {
		when(daoServiceGame.isFull(gameWithPlayer1.getIdGame()))
		.thenReturn(true);
		try{
			AccueilControllerImpl.joinGame(gameWithPlayer1.getIdGame(), player2.getName(), "userKey");
			assert false;
		}catch(Exception e){
			assert true;
		}
	
	}
	
	@Test
	public void testChoixHeroDeuxiemePlayerAndInitGame() throws AlreadyHaveHeroException, UnknowHeroException {
		player1.setHero(heroFactory.createHero(HeroFactory.PALADIN));
		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		game.setIdGame((long) 1);
		when(daoServicePlayer.alreadyHaveHero(game.getPlayer2().getIdPlayer())).thenReturn(false);
		assertNull(game.getPlayer2().getHero());
		when(daoServiceGame.get(game.getIdGame())).thenReturn(addHeroToPlayer2In(game));
		
		AccueilControllerImpl.setHero(game.getIdGame(), game.getPlayer2().getIdPlayer(), HeroFactory.PALADIN);
		assertNotNull(game.getPlayer2().getHero());	
	}

	
	private Game addHeroToPlayer2In(Game game) throws UnknowHeroException {
		game.getPlayer2().setHero(heroFactory.createHero(HeroFactory.PALADIN));
		return game;
	}

	@Test
	public void testChoixHeroFirstPlayer() throws AlreadyHaveHeroException, UnknowHeroException {
		when(daoServicePlayer.alreadyHaveHero(gameWithPlayer1.getPlayer1().getIdPlayer())).thenReturn(false);
		assertNull(gameWithPlayer1.getPlayer1().getHero());
		when(daoServiceGame.get(gameWithPlayer1.getIdGame())).thenReturn(addHeroToPlayer1In(gameWithPlayer1));
		AccueilControllerImpl.setHero(gameWithPlayer1.getIdGame(), gameWithPlayer1.getPlayer1().getIdPlayer(), HeroFactory.PALADIN);
		assertNotNull(gameWithPlayer1.getPlayer1().getHero());	
		verify(turnManager, never()).initGame(gameWithPlayer1);
		verify(turnManager, never()).activeTimer(gameWithPlayer1.getIdGame());
	}

	private Game addHeroToPlayer1In(Game gameWithPlayer1) throws UnknowHeroException {
		 gameWithPlayer1.getPlayer1().setHero(heroFactory.createHero(HeroFactory.PALADIN));
		return gameWithPlayer1;
	}
	
	Hero heroPlayer1;
	Hero heroPlayer2;
	@Test
	public void testGetHand() throws UnknowHeroException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();

		ArrayList<String> listHandPlayer1 = new ArrayList<String>();
		listHandPlayer1.add(CardFactory.BENEDICTION_DE_PUISSANCE);
		
 		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setHand(listHandPlayer1);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);
		
		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		
		List<String> result = AccueilControllerImpl.getHand(game.getIdGame(), player1.getIdPlayer());
		assertEquals(1, result.size());
		assertEquals(CardFactory.BENEDICTION_DE_PUISSANCE, result.get(0));
	}
}
