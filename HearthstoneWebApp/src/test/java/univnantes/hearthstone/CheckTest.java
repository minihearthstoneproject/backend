package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceEntity;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.util.TurnManager;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
@ComponentScan("univnantes.hearthstone")
public class CheckTest  {
	
	@Mock
	protected DaoServiceGame daoServiceGame;

	@Mock
	protected DaoServiceEntity daoServiceEntity;

	@Mock
	protected DaoServicePlayer daoServicePlayer;

	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;

	@Autowired
	@InjectMocks
	HeroFactory heroFactory;

	@Autowired
	@InjectMocks
	CardFactory cardFactory;

	@Mock
	TurnManager turnManager;

	@Mock
	Sauvegarde sauvegarde;

	Hero heroPlayer1;
	Hero heroPlayer2;

	protected Player player1;
	protected Player player2;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);

		player1 = new Player("user1", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key");
		player2 = new Player("user2", null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key2");
	}
	
	/**
	 * test check, le tour passe bien au joueur suivant
	 * @throws CantPlayException
	 * @throws InterruptedException 
	 * @throws UnknowHeroException 
	 */
	@Test
	public void testCheck() throws CantPlayException, InterruptedException, UnknowHeroException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		
 		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		
		Game game = new Game(player1, player2, (byte) 1, new Date().getTime()-2 , "key");
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		
		assertEquals((byte) 1, game.getPlayerTurn());
		
		gameControllerImpl.check(player1.getIdPlayer(), game.getIdGame());
		
		verify(daoServiceGame,  times(2)).get(game.getIdGame());
		verify(daoServiceGame, times(1)).turnTimeOut(game.getIdGame());
		verify(turnManager,times(1)).initTurn(game.getIdGame());
		verify(turnManager,times(1)).activeTimer(game.getIdGame());
	}
	
}
