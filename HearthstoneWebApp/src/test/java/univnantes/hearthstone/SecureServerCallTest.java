package univnantes.hearthstone;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.controller.Impl.WebSocketController;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.util.TurnManager;
import univnantes.hearthstone.util.clientMessages.CheckMessage;

@SpringBootTest(classes = HearthstoneWebAppApplication.class)
@RunWith(SpringRunner.class)
@ComponentScan("univnantes.hearthstone")
public class SecureServerCallTest extends UtilsTest{

	@Autowired
	@InjectMocks
	WebSocketController webSocketController;
	
	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;
	
	
	@Autowired
	@InjectMocks
	TurnManager turnManager;
	
	
	@Autowired
	@InjectMocks
	PiocheManager piocheManager;
	
	@Autowired
	HeroFactory heroFactory;
	
	@Test
	public void testCallOk() throws UnknowHeroException {
		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroFactory.createHero(HeroFactory.PALADIN));
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroFactory.createHero(HeroFactory.PALADIN));

		Game game = new Game(player1, player2, (byte) 1, null, "key");
		game.setIdGame(Long.valueOf(1));
		
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceGame.turnTimeOut(game.getIdGame())).thenReturn(game);
		CheckMessage m = new CheckMessage(game.getIdGame(), game.getkey(), player1.getIdPlayer(),  player1.getKey());
		try {
			webSocketController.onCheck(game.getIdGame(), m);
			assert true;
		} catch (Exception e) {
			e.printStackTrace();
			assert false;
		}
	}
	

	@Test
	public void testCallKOWithPlayerKey() {
		player1.setIdPlayer(Long.valueOf(1));
		player2.setIdPlayer(Long.valueOf(2));
		Game game = new Game(player1, player2, (byte) 1, null, "key");
		game.setIdGame(Long.valueOf(1));
		
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceGame.turnTimeOut(game.getIdGame())).thenReturn(changeTurnGame(game, (byte) 2));
		CheckMessage m = new CheckMessage(game.getIdGame(), game.getkey(), player1.getIdPlayer(),  "faux");
		try {
			webSocketController.onCheck(game.getIdGame(), m);
			assert false;
		} catch (Exception e) {
			e.printStackTrace();
			assert true;
		}
	}
	
	@Test
	public void testCallKOWithGameKey() {
		player1.setIdPlayer(Long.valueOf(1));
		player2.setIdPlayer(Long.valueOf(2));
		Game game = new Game(player1, player2, (byte) 1, null, "key");
		game.setIdGame(Long.valueOf(1));
		
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);
		when(daoServiceGame.turnTimeOut(game.getIdGame())).thenReturn(changeTurnGame(game, (byte) 2));
		CheckMessage m = new CheckMessage(game.getIdGame(), "faux", player1.getIdPlayer(),  player1.getKey());
		try {
			webSocketController.onCheck(game.getIdGame(), m);
			assert false;
		} catch (Exception e) {
			e.printStackTrace();
			assert true;
		}
	}
}
