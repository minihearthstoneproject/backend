package univnantes.hearthstone;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;

import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.factories.HeroFactory;
import univnantes.hearthstone.util.TurnManager;

public class ForfeitTest extends UtilsTest {

	@Autowired
	@InjectMocks
	private GameControllerImpl gameControllerImpl;
	
	@Autowired
	@InjectMocks
	HeroFactory heroFactory;
	
	@Autowired
	@InjectMocks
	CardFactory cardFactory;
	
	@Spy
	private TurnManager turnManager;
	
	Hero heroPlayer1;
	Hero heroPlayer2;
	
	@Test
	public void testForfeit() throws UnknowHeroException, UnknowCardException, CantPlayException {
		heroPlayer1 = heroFactory.createHero(HeroFactory.MAGE);
		heroPlayer1.setCanAttack(true);
		heroPlayer1.setIdEntity(Long.valueOf(1));
		heroPlayer1.setHealthPoint(10);
		heroPlayer2 = heroFactory.createHero(HeroFactory.PALADIN);
		heroPlayer2.setIdEntity(Long.valueOf(2));
		heroPlayer2.setHealthPoint(10);

		ArrayList<Entity> listEntityPlayer1 = new ArrayList<Entity>();
		listEntityPlayer1.add((Servant) cardFactory.createCard(CardFactory.YETI_NOROIT));
		ArrayList<Entity> listEntityPlayer2 = new ArrayList<Entity>();
		listEntityPlayer2.add((Servant) cardFactory.createCard(CardFactory.SOLDAT_DU_COMPTE_DE_L_OR));

		player1.setIdPlayer(Long.valueOf(1));
		player1.setHero(heroPlayer1);
		player1.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player1.setPlayingCard(listEntityPlayer1);
		player2.setIdPlayer(Long.valueOf(2));
		player2.setHero(heroPlayer2);
		player2.setMana(DaoServicePlayer.MAX_MANA_PLAYER_INIT);
		player2.setPlayingCard(listEntityPlayer2);
		
		Game game = new Game(player1, player2, DaoServiceGame.FIRST_PLAYER_INIT, null, null);
		game.setIdGame((long) 1);
		when(daoServiceGame.get(game.getIdGame())).thenReturn(game);

		Game result = gameControllerImpl.forfeit(game.getIdGame(), game.getPlayer1().getIdPlayer());
		verify(turnManager, times(1)).removeTimer(game.getIdGame());

		assertEquals(0,result.getPlayer1().getHero().getHealPoint());
	}
}
