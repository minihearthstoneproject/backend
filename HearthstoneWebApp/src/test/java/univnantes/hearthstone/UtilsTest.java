package univnantes.hearthstone;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import univnantes.hearthstone.dao.services.impl.DaoServiceEntity;
import univnantes.hearthstone.dao.services.impl.DaoServiceGame;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.factories.HeroFactory;

public abstract class UtilsTest {

	protected static final String NAME_J1 = "namej1";
	protected static final String NAME_J2 = "namej2";
	
	@Mock
	protected DaoServiceGame daoServiceGame;
	
	@Mock
	protected DaoServiceEntity daoServiceEntity;

	@Mock
	protected DaoServicePlayer daoServicePlayer;

	@InjectMocks
	protected HeroFactory heroFactory;
	
	protected Player player1;

	protected Player player2;
	
	protected Game gameWithPlayer1;
	
	/**
	 * initialisation des mocks et des variables
	 * player1
	 * player2
	 * gameWithPlayer1
	 */
	@Before
	public void setupMock() {

		MockitoAnnotations.initMocks(this);

		player1 = new Player(NAME_J1, null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key");
		player2 = new Player(NAME_J2, null, new ArrayList<String>(),
				new ArrayList<univnantes.hearthstone.modele.Entity>(), DaoServicePlayer.MANA_PLAYER_INIT,
				DaoServicePlayer.MAX_MANA_PLAYER_INIT, "key2");
		gameWithPlayer1 = new Game(player1, null, DaoServiceGame.FIRST_PLAYER_INIT, null, "key");

		when(daoServicePlayer.createPlayer(player1.getName(), "userKey")).thenReturn(player1);
		when(daoServicePlayer.createPlayer(player2.getName(),"userKey")).thenReturn(player2);
		when(daoServiceGame.createGameWithPlayer("gameKey", player1)).thenReturn(gameWithPlayer1);
		when(daoServiceGame.joinGameWithId(gameWithPlayer1.getIdGame(), player2))
				.thenReturn(addPlayer2ToGame(gameWithPlayer1, player2));
		
	}

	/**
	 * permet d'ajouter un joueur a une partie sans passer par la base de donnée (pour les tests)
	 * @param game
	 * @param player
	 * @return
	 */
	protected Game addPlayer2ToGame(Game game, Player player) {
		game.setPlayer2(player);
		return game;
	}
	
	protected Game changeTurnGame(Game game, byte turn) {
		game.setPlayerTurn(turn);
		return game;
	}
	
	protected Player playeraddhandcard(Player player) {
		 player.addHandCard("string");
		return player;
	}
}
