package univnantes.hearthstone.util.clientMessages;

public class CheckMessage extends SecureClientMessage{

    public CheckMessage()
    {

    }
    public CheckMessage(Long gameId, String gameKey, Long playerId, String playerKey) {
        super(gameId, gameKey, playerId, playerKey);
    }

}
