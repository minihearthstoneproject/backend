package univnantes.hearthstone.util.clientMessages;

public class ActionMessage extends SecureClientMessage {

    private Long sourceId;

    private Long targetId;

    public ActionMessage()
    {

    }

    public ActionMessage(Long gameId, Long playerId, String gameKey, String playerKey, Long sourceId, Long targetId) {
        super(gameId,gameKey,playerId,playerKey);
        this.sourceId = sourceId;
        this.targetId = targetId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public Long getTargetId() {
        return targetId;
    }
}
