package univnantes.hearthstone.util.clientMessages;

public class CreateGameMessage {

    private String username;

    private String key;

    public CreateGameMessage(String username,String key)
    {
        this.username = username;
        this.key = key;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getKey()
    {
        return this.key;
    }
}
