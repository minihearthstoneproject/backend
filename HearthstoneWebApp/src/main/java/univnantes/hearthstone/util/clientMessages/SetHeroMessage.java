package univnantes.hearthstone.util.clientMessages;

public class SetHeroMessage extends SecureClientMessage{

    private String hero;

    public SetHeroMessage(){

    }
    public SetHeroMessage(Long gameId, String gameKey, Long playerId, String playerKey, String hero) {
        super(gameId, gameKey, playerId, playerKey);
        this.hero = hero;
    }

    public String getHero() {
        return hero;
    }

    public void setHero(String hero) {
        this.hero = hero;
    }
}
