package univnantes.hearthstone.util.clientMessages;

public abstract class SecureClientMessage {

    private Long gameId;

    private String gameKey;

    private Long playerId;

    private String playerKey;

    public SecureClientMessage(){

    }

    public SecureClientMessage(Long gameId, String gameKey, Long playerId, String playerKey) {
        this.gameId = gameId;
        this.gameKey = gameKey;
        this.playerId = playerId;
        this.playerKey = playerKey;
    }

    public Long getGameId() {
        return gameId;
    }

    public String getGameKey() {
        return gameKey;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public String getPlayerKey() {
        return playerKey;
    }
}
