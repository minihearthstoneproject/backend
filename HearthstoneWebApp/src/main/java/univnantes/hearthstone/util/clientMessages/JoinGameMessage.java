package univnantes.hearthstone.util.clientMessages;

public class JoinGameMessage {

    private String username;

    private String gameKey;
    
    private String userKey;

    public JoinGameMessage(){

    }

    public JoinGameMessage(String gameKey,String username,String userKey)
    {
        this.gameKey = gameKey;
        this.username = username;
        this.userKey = userKey;
    }

    public String getGameKey() {
        return gameKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGameKey(String gameKey) {
        this.gameKey = gameKey;
    }

	public String getUserKey() {
		return userKey;
	}
	
	public void setUserKey(String userKey) {
	        this.userKey = userKey;
	    }

}
