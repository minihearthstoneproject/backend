package univnantes.hearthstone.util.clientMessages;

public class ForfeitMessage extends SecureClientMessage{

    public ForfeitMessage()
    {

    }

    public ForfeitMessage(Long gameId, String gameKey, Long playerId, String playerKey) {
        super(gameId, gameKey, playerId, playerKey);
    }
}
