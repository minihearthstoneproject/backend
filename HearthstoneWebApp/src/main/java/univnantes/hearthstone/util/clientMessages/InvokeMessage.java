package univnantes.hearthstone.util.clientMessages;

public class InvokeMessage extends SecureClientMessage {

    private String cardName;
	private Long targetId;

	public InvokeMessage()
    {

    }

    public InvokeMessage(Long gameId, String gameKey, Long playerId, String playerKey, String cardName) {
        super(gameId, gameKey, playerId, playerKey);
        this.cardName = cardName;
    }

    public String getCardName() {
        return cardName;
    }

	public Long getTargetId() {
		return targetId;
	}
}
