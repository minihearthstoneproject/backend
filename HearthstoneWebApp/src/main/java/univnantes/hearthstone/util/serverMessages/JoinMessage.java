package univnantes.hearthstone.util.serverMessages;

public class JoinMessage extends StandardMessage {

    private Long playerId;

    private Long gameId;

    public JoinMessage(Long playerId,Long gameId)
    {
        this.playerId = playerId;
        this.gameId = gameId;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public Long getGameId() {
        return gameId;
    }
}
