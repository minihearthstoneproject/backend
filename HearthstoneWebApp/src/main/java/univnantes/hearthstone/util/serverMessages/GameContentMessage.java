package univnantes.hearthstone.util.serverMessages;

import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.util.metaClasses.MetaHero;
import univnantes.hearthstone.util.metaClasses.MetaServant;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameContentMessage extends StandardMessage {

   List<MetaServant> playingCards1 = new ArrayList<>();

   List<MetaServant> playingCards2 = new ArrayList<>();

   MetaHero hero1;

   MetaHero hero2;

   int mana1;

   int mana2;

   int turn;

   boolean active;

   boolean allPlayersConnected;

   Long callerId;

   Long player1Id;

   Long player2Id;

   Long timerStart;

    public GameContentMessage(Game theGame){

    }

    public GameContentMessage(Game theGame,Long playerId)
    {
        List<Servant> tempPlayingCards1 = new ArrayList<>();
        List<Servant> tempPlayingCards2 = new ArrayList<>();

        player1Id = theGame.getPlayer1().getIdPlayer();

        if(playerId == player1Id)
        {
            callerId = player1Id;
        }else{
            callerId = theGame.getPlayer2().getIdPlayer();
        }

        // On vérfie que le 2ème joueur s'est inscrit
        if(theGame.getPlayer2()!=null) {

            player2Id = theGame.getPlayer2().getIdPlayer();

            allPlayersConnected = true;

            // On signale que les 2 joueurs sont inscrits et ont selectionné leur héros
            if(theGame.getPlayer2().getHero()!=null) {
                active = true;
            }else{
                active = false;
            }

            // Récuperation des informations sur le tour: joueur attendu et temps de démarrage
            turn = theGame.getPlayerTurn();

            // Récuperation de la main et des carte sur plateau des joueurs
            if ((playerId == player1Id || playerId == player2Id) && this.active) {

                System.out.println("timerStart: "+timerStart);
                timerStart = theGame.getdate();

                mana1 = theGame.getPlayer1().getMana();

                mana2 = theGame.getPlayer2().getMana();

                // Récuperation des informations sur les héros
                hero1 = new MetaHero(
                        theGame.getPlayer1().getHero().getIdHero(),
                        theGame.getPlayer1().getHero().getArmor(),
                        theGame.getPlayer1().getHero().getHealthPoint(),
                        theGame.getPlayer1().getHero().isCanAttack(),
                        theGame.getPlayer1().getHero().getName());

                hero2 = new MetaHero(
                        theGame.getPlayer2().getHero().getIdHero(),
                        theGame.getPlayer2().getHero().getArmor(),
                        theGame.getPlayer2().getHero().getHealthPoint(),
                        theGame.getPlayer2().getHero().isCanAttack(),
                        theGame.getPlayer2().getHero().getName());

                // Conteneurs temporaires
                tempPlayingCards1 = (List<Servant>)(List<?>) theGame.getPlayer1().getPlayingCard();
                tempPlayingCards2 = (List<Servant>)(List<?>) theGame.getPlayer2().getPlayingCard();

                if (tempPlayingCards1 != null) {

                    this.playingCards1 = tempPlayingCards1.stream().map(x -> new MetaServant(x.getIdEntity(), x.getName(), x.getHealthPoint(), x.getAttack(), x.isCanAttack())).collect(Collectors.toList());
                }
                if (tempPlayingCards2 != null) {
                    this.playingCards2 = tempPlayingCards2.stream().map(x -> new MetaServant(x.getIdEntity(), x.getName(), x.getHealthPoint(), x.getAttack(), x.isCanAttack())).collect(Collectors.toList());
                }

            }else {
                // Cas dans lequel un id de joueur obtenu n'appartient pas à la partie
                System.out.println("GameContentMessage: Accès illégal detecté: ID de joueur non reconnu.");
            }
        }else{
            allPlayersConnected = false;
        }
    }

    public List<MetaServant> getPlayingCards1() {
        return playingCards1;
    }

    public List<MetaServant> getPlayingCards2() {
        return playingCards2;
    }

    public MetaHero getHero1() {
        return hero1;
    }

    public MetaHero getHero2() {
        return hero2;
    }

    public int getMana1() {
        return mana1;
    }

    public int getMana2() {
        return mana2;
    }

    public int getTurn() {
        return turn;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isAllPlayersConnected() {
        return allPlayersConnected;
    }

    public Long getCallerId() {
        return callerId;
    }

    public Long getPlayer1Id() {
        return player1Id;
    }

    public Long getPlayer2Id() {
        return player2Id;
    }

    public Long getTimerStart() {
        return timerStart;
    }
}
