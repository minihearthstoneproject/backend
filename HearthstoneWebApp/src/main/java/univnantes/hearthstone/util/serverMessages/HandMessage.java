package univnantes.hearthstone.util.serverMessages;

import java.util.ArrayList;
import java.util.List;

public class HandMessage extends StandardMessage {

    List<String> hand = new ArrayList<>();

    public HandMessage(List<String> hand) {
        this.hand = hand;
    }

    public List<String> getHand() {
        return hand;
    }

    public void setHand(List<String> hand) {
        this.hand = hand;
    }
}
