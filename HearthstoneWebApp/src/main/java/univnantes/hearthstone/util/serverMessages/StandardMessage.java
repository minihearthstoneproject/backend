package univnantes.hearthstone.util.serverMessages;

public abstract class StandardMessage {

    private String type = "standard";

    public String getType() {
        return type;
    }
}
