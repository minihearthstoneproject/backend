package univnantes.hearthstone.util.metaClasses;

public class MetaServant {

    private Long id;

    private String name;

    private int healthPoint;

    private int attack;

    private boolean canAttack;

    public MetaServant(Long id, String name, int healthPoint,int attack, boolean canAttack) {
        this.id = id;
        this.name = name;
        this.healthPoint = healthPoint;
        this.attack = attack;
        this.canAttack = canAttack;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealthPoint() {
        return healthPoint;
    }

    public void setHealthPoint(int healthPoint) {
        this.healthPoint = healthPoint;
    }

    public boolean isCanAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }
}
