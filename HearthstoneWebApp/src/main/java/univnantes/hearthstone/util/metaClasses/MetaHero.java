package univnantes.hearthstone.util.metaClasses;

public class MetaHero {

    Long id;

    int armorLeft;

    int hpLeft;

    boolean canAttack;

    String name;

    public MetaHero(Long id, int armorLeft, int hpLeft,boolean canAttack, String name) {
        this.id = id;
        this.armorLeft = armorLeft;
        this.hpLeft = hpLeft;
        this.canAttack = canAttack;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public int getArmorLeft() {
        return armorLeft;
    }

    public int getHpLeft() {
        return hpLeft;
    }

    public String getName() {
        return name;
    }

    public boolean isCanAttack() {
        return canAttack;
    }
}
