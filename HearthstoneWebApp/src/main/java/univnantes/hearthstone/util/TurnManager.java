package univnantes.hearthstone.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.dao.services.IDaoServiceEntity;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.dao.services.impl.DaoServicePlayer;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.util.serverMessages.GameContentMessage;

@Component
public class TurnManager {

	public static Logger logger = Logger.getLogger("TurnManager");

	@Autowired
	private IDaoServicePlayer daoServicePlayer;

	@Autowired
	private IDaoServiceGame daoServiceGame;

	@Autowired
	private IDaoServiceEntity daoServiceEntity;

	@Autowired
	private PiocheManager piocheManager;

	@Autowired
	private SimpMessagingTemplate webSocket;

	public static final int DELAY_FOR_ONE_TURN = 120000;// temps en milliseconde

	private Map<Long, Timer> timerList = new HashMap<Long, Timer>();;

	/**
	 * initialise une map de
	 */
	public TurnManager() {
	}

	/**
	 * active le timer pour gerer le temps maximum que dure un tour
	 */
	public void activeTimer(Long idGame) {
		daoServiceGame.setTimeTurn(idGame);
		clearTimer(idGame);// remise a 0 si cela n'a ps ete fait

		timerList.get(idGame).schedule(new TimerTask() {
			@Override
			public void run() {
				daoServiceGame.turnTimeOut(idGame);
				initTurn(idGame);
				Game g = daoServiceGame.get(idGame);
				Long playerIdNewTurn = g.getPlayerTurn() == ((byte) 1) ? g.getPlayer1().getIdPlayer()
						: g.getPlayer2().getIdPlayer();
				String channel = "/game/" + idGame;
				g.setdate(new Date().getTime());
				webSocket.convertAndSend(channel, new GameContentMessage(g, playerIdNewTurn));
				activeTimer(g.getIdGame());
			}
		}, DELAY_FOR_ONE_TURN);
	}

	/**
	 * supprime le timer
	 */
	public void removeTimer(Long idGame) {
		if (timerList.containsKey(idGame)) {
			timerList.get(idGame).cancel();
			timerList.remove(idGame);
		}
	}

	/**
	 * remise a 0 du timer
	 * 
	 * @param idGame
	 */
	public void clearTimer(Long idGame) {
		if (timerList.containsKey(idGame)) {
			timerList.get(idGame).cancel();
			timerList.replace(idGame, new Timer());
		} else {
			timerList.put(idGame, new Timer());
		}
	}

	public Map<Long, Timer> getTimerList() {
		return timerList;
	}

	public void activeTimerTest(Long idGame, int timeMs) {

		clearTimer(idGame);// remise a 0 si cela n'a ps ete fait

		timerList.get(idGame).schedule(new TimerTask() {
			@Override
			public void run() {
				Game g = daoServiceGame.turnTimeOut(idGame);
				initTurn(idGame);
			}
		}, timeMs);
	}

	/**
	 * initialise le tour courrant (pioche, tous les serviteurs peuvent attacker,
	 * ...)
	 * 
	 * @param idGame
	 * @throws UnknowHeroException
	 */
	public void initTurn(Long idGame) {
		try {
			Game game = daoServiceGame.get(idGame);
			piocheManager.piocheOneCard(game);
			Player player = (game.getPlayerTurn() == (byte) 1) ? game.getPlayer1() : game.getPlayer2();
			player.getPlayingCard().iterator().forEachRemaining(entity -> daoServiceEntity.canAttackTrue(entity));
			daoServiceEntity.canAttackTrue(player.getHero());
			//			player.setHero((Hero) daoServiceEntity.canAttackTrue(player.getHero()));

			if (player.getMaxMana() < DaoServicePlayer.MAX_MANA_PLAYER_INIT) {
				player.incremMaxMana();
			}

			player.setMana(player.getMaxMana());

			daoServicePlayer.save(player);
			daoServiceGame.saveGame(game);

		} catch (UnknowHeroException e) {
			logger.warning(e.toString());
		}
	}

	/**
	 * initialise les mains des joueurs audebut de la partie (3par personnes) ->
	 * deuxieme joueur commencera sont tour avec 4 cartes
	 * 
	 * @param game
	 * @throws UnknowHeroException
	 */
	public void initGame(Game game) throws UnknowHeroException {
		Random r = new Random();
		byte firstPlayer = (byte) (1 + r.nextInt(2));
		game.setPlayerTurn(firstPlayer);
		Player player = (game.getPlayerTurn() == (byte) 1) ? game.getPlayer1() : game.getPlayer2();
		player.setMana(1);
		player.incremMaxMana();
		piocheManager.piocheInitGame(game);
		daoServiceGame.saveGame(game);
	}

}
