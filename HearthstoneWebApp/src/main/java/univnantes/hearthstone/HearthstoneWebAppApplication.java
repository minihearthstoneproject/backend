package univnantes.hearthstone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HearthstoneWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HearthstoneWebAppApplication.class, args);
    }
}
