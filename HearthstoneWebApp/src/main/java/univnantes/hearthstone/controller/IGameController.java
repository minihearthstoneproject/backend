package univnantes.hearthstone.controller;

import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.DontHaveTheCardException;
import univnantes.hearthstone.exception.EnoughManaException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.util.TurnManager;

public interface IGameController {

	/**
	 * invoke la carte name pour le joueur a qui est le tour dans la game idGame
	 * 
	 * @param cardName, idGame
	 * @return
	 * @throws DontHaveTheCardException 
	 * @throws CantPlayException 
	 * @throws UnknowCardException 
	 * @throws EnoughManaException 
	 * @throws UnknowHeroException 
	 */
	Game invokeCard(Long idPlayer, Long idGame, String cardName, Long idTarget) throws DontHaveTheCardException, CantPlayException, UnknowCardException, EnoughManaException, UnknowHeroException;

	/**
	 * execute l'action du joueur idExcecutor
	 * 
	 * @param idGame
	 * @param idExcecutor
	 * @param idTarget
	 * @return
	 * @throws CantPlayException 
	 * @throws UnknowHeroException 
	 */
	Game action(Long idPlayer, Long idGame, Long idExcecutor, Long idTarget) throws CantPlayException, UnknowHeroException;

	/**
	 * passe le tour du joueur en question
	 * 
	 * @param idGame
	 * @return
	 * @throws CantPlayException 
	 * @throws UnknowHeroException 
	 */
	Game check(Long idPlayer, Long idGame) throws CantPlayException, UnknowHeroException;

	/**
	 * joueur declare forfet
	 * 
	 * @param idGame
	 * @param idPlayer
	 * @return
	 * @throws CantPlayException 
	 */

	Game forfeit(Long idGame, Long idPlayer) throws CantPlayException;
	
	/**
	 * retourne le turnManager
	 * @return
	 */
	TurnManager getTurnManager();
}
