package univnantes.hearthstone.controller;


import univnantes.hearthstone.exception.AlreadyHaveHeroException;
import univnantes.hearthstone.exception.GameAlreadyFullException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;

import java.util.List;

public interface IAccueilController {

	

	/**
	 * creer une nouvelle partie, affecte un identifiant de joueur lié au nomUtilisateur.
	 * 
	 * @return la nouvelle partie. Elle contient l'identifiant du joueur qui vient de la créer
	 */
	Game createGame(String gameKey, String userName, String userKey);
	
	
	/**
	 * permet de rejoindre la partie ayant l'identifiant passée en parametre
	 * 
	 * @pre la partie n'a un seul et unique joueur d'affectée
	 * 
	 * @return la nouvelle partie. Elle contient l'identifiant du joueur qui vient de la rejoindre
	 *			ou alors un avertissement comme quoi la partie n'existe pas
	 * @throws GameAlreadyFullException**/ 
	Game joinGame(Long idGame, String userName, String key) throws GameAlreadyFullException;
	
	/**
	 * permet de choisir sont hero
	 * 	 * 
	 * @return la partie. 
	 * @throws UnknowHeroException 
	 * @throws AlreadyHaveHeroException 
	 * */
	Game setHero( Long idGame, Long idUser, String hero) throws UnknowHeroException, AlreadyHaveHeroException;

	/**
	 *
	 * @param idGame
	 * @param idUser
	 * @return Liste des cartes du joueur
	 */
	List<String> getHand(Long idGame, Long idUser);
}
