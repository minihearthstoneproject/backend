package univnantes.hearthstone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import univnantes.hearthstone.controller.Impl.AccueilControllerImpl;
import univnantes.hearthstone.controller.Impl.GameControllerImpl;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.util.PiocheManager;
import univnantes.hearthstone.exception.AlreadyHaveHeroException;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.DontHaveTheCardException;
import univnantes.hearthstone.exception.EnoughManaException;
import univnantes.hearthstone.exception.GameAlreadyFullException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.factories.HeroFactory;

@Controller
@RequestMapping("/test/")
public class TestController {
	
	Long idGame;
	Long idUser1;

	@Autowired
	AccueilControllerImpl accueilControllerImpl;
	
	@Autowired
	private IDaoServiceGame daoServiceGame;
	
	@Autowired
	GameControllerImpl gameControllerImpl;
	
	@Autowired
	PiocheManager piocheManager;

	@GetMapping("enregistrer")
	@ResponseBody
	Object enregistrer() {
		System.out.println("enregistrement");
		 Game game =  accueilControllerImpl.createGame("gameKey", "tttt", "userKey");
		 idGame = game.getIdGame();
		 idUser1 = game.getPlayer1().getIdPlayer();
		 return game;
	}
	
	@GetMapping("retrouver")
	@ResponseBody
	Object retrouver() {
		System.out.println("aller chercher element");
		assert(idGame != null);
		return daoServiceGame.get(idGame);
	}
	
	@GetMapping("setHero")
	@ResponseBody
	Object setH()  {
		System.out.println("set hero");
		assert(idGame!= null);
		assert(idUser1!= null);
		Game game = null;
		try {
			game = accueilControllerImpl.setHero(idGame, idUser1, HeroFactory.GUERRIER);
		} catch (AlreadyHaveHeroException | UnknowHeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return game;
	}
	
	@GetMapping("join")
	@ResponseBody
	Object joinGame() {
		System.out.println("join");
		assert(idGame!=null);
		Game game = null;
		try {
			game = accueilControllerImpl.joinGame(idGame, "bbbb", "userKey");
		} catch (GameAlreadyFullException e) {
			System.out.println("erreur ->  "+e);

		}
		return game;
	}
	
	@GetMapping("invoke")
	@ResponseBody
	Object invoke() throws UnknowHeroException {
		
		Game game=null;

		System.out.println("join");
		game = daoServiceGame.get(idGame);
		game.getPlayer1().setMana(10);
		daoServiceGame.saveGame(game);
		try {
			piocheManager.piocheOneCard(game);
		} catch (UnknowHeroException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			game = gameControllerImpl.invokeCard(idUser1, idGame, game.getPlayer1().getHand().get(0), null);
		} catch (DontHaveTheCardException | CantPlayException | UnknowCardException | EnoughManaException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return game;
	}
}