package univnantes.hearthstone.controller.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.controller.IGameController;
import univnantes.hearthstone.dao.repository.EntityRepository;
import univnantes.hearthstone.dao.services.IDaoServiceEntity;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.dao.util.Sauvegarde;
import univnantes.hearthstone.exception.CantPlayException;
import univnantes.hearthstone.exception.DontHaveTheCardException;
import univnantes.hearthstone.exception.EnoughManaException;
import univnantes.hearthstone.exception.UnknowCardException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Entity;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.Servant;
import univnantes.hearthstone.modele.Spell;
import univnantes.hearthstone.modele.factories.CardFactory;
import univnantes.hearthstone.modele.specialAction.CurrentAction;
import univnantes.hearthstone.modele.specialAction.DeadAction;
import univnantes.hearthstone.modele.specialAction.InvokeAction;
import univnantes.hearthstone.util.TurnManager;

@Component
public class GameControllerImpl implements IGameController {

	@Autowired
	private IDaoServiceGame daoServiceGame;

	@Autowired
	private IDaoServicePlayer daoServicePlayer;

	@Autowired
	private IDaoServiceEntity daoServiceEntity;

	@Autowired
	private Sauvegarde daoSauvegarde;

	@Autowired
	private CardFactory cardFactory;

	@Autowired
	private TurnManager turnManager;;

	@Override
	public Game invokeCard(Long idPlayer, Long idGame, String cardName, Long idTarget) throws DontHaveTheCardException,
			CantPlayException, UnknowCardException, EnoughManaException, UnknowHeroException {
		// verifie si le joueur peut jouer
		Game game = daoServiceGame.get(idGame);
		Player player = getPlayerIfCanPlay(game, idPlayer);
		Player playerEnemy = getPlayerEnemy(game, player);

		// vérifier que le joueur peut jouer cette carte (dans son jeu, un serviteur ou
		// un sort,
		// assez de mana)
		if (!playerHaveCardInHisHand(player, cardName))
			throw new DontHaveTheCardException(cardName);
		Entity card = cardFactory.createCard(cardName);

		// execution
		if (card instanceof Servant) {
			Servant servant = (Servant) card;
			if (!enoughMana(player, servant.getInvokeCost()))
				throw new EnoughManaException();
			daoServicePlayer.StealManaPlayer(player, servant.getInvokeCost());// enleve les point de mana necessaire a
																				// l'invokation
			daoServicePlayer.invokeCard(player, servant);// ajoute le servant au cartes jouer et supprime la carte de la
															// main
			// execution des actions spéciales à l'invocation
			servant.getSpecialAction().iterator().forEachRemaining(sp -> {
				if (sp instanceof InvokeAction) {
					try {
						sp.execute(daoSauvegarde, game, servant, null);
					} catch (UnknowCardException | UnknowHeroException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		} else if (card instanceof Spell) {
			Spell spell = (Spell) card;
			if (!enoughMana(player, spell.getUseCost()))
				throw new EnoughManaException();
			daoServicePlayer.StealManaPlayer(player, spell.getUseCost());// enleve les point de mana necessaire a
																			// l'invokation
			Entity target = (idTarget == null) ? null : getAndVerifyEntity(game, idTarget);

			if (target != null) {
				boolean haveProvocation = haveProvocation(playerEnemy);
				boolean targetIsEnemy = playerEnemy.getPlayingCard().contains(target);
				// joueur
				if (targetIsEnemy && haveProvocation && (target instanceof Servant)
						&& !((Servant) target).isProvocation())
					throw new IllegalArgumentException(
							target.getIdEntity() + " n'a pas un atout provocation alors qued'autre oui");
				if (targetIsEnemy && haveProvocation && (target instanceof Hero))
					throw new IllegalArgumentException(
							target.getIdEntity() + " serviteur avec provocation vous ne pouvez pas attaquer le hero");
			}

			spell.getSpecialAction().execute(daoSauvegarde, game, spell, target);

			// supprime la carte de la main
			daoServicePlayer.removeHandCard(player, card);

			if (idTarget != null) {
				if (daoServiceEntity.isDead(idTarget)) {// servant mort -> suppression du servant + actionSpecial
					deadProcess(game, playerEnemy, target);

				}
			}else {
				processIfDead(game, playerEnemy);
			}
		}

		return daoServiceGame.get(idGame);

	}

	@Override
	public Game action(Long idPlayer, Long idGame, Long idExcecutor, Long idTarget)
			throws CantPlayException, UnknowHeroException {
		// verifie si le joueur peut jouer
		Game game = daoServiceGame.get(idGame);
		Player player = getPlayerIfCanPlay(game, idPlayer);
		Player playerEnemy = getPlayerEnemy(game, player);

		// verifie si les entite peuvent etre jouée
		Entity excecutor = (idExcecutor == null) ? null : getAndVerifyEntity(player, idExcecutor);// recuperation de l'entite excecutante
		if (!excecutor.isCanAttack())
			throw new CantPlayException();// jouable
		Entity target = (idTarget == null) ? null :  getAndVerifyEntity(playerEnemy, idTarget);// recuperation de la cible et vérifie qu'elle
																	// appartient au
		boolean haveProvocation = haveProvocation(playerEnemy);

		// joueur
		if (haveProvocation && (target instanceof Servant) && !((Servant) target).isProvocation())
			throw new IllegalArgumentException(
					target.getIdEntity() + " n'a pas un atout provocation alors qued'autre oui");
		if (haveProvocation && (target instanceof Hero))
			throw new IllegalArgumentException(
					target.getIdEntity() + " serviteur avec provocation vous ne pouvez pas attaquer le hero");

		// execution
		Game gameResult = null;
		if (excecutor instanceof Hero) {
			try {
				((Hero) excecutor).getSpecialAction().execute(daoSauvegarde, game, excecutor, target);
			} catch (UnknowCardException e) {
				e.printStackTrace();
			}

			excecutor.setCanAttack(false);
			daoSauvegarde.save(excecutor);
			daoServicePlayer.StealManaPlayer(player, 2);// enleve les point de mana necessaire a

			if(target == null) {
				gameResult = processIfDead(game, playerEnemy);
			}else {
				if (daoServiceEntity.isDead(idTarget)) {// servant mort -> suppression du servant + actionSpecial
					gameResult = deadProcess(game, playerEnemy, target);
				} else {
					gameResult = daoServiceGame.get(game.getIdGame());
				}
			}
			
			
		} else if (excecutor instanceof Servant) {
			// actions spéciales du servant
			((Servant) excecutor).getSpecialAction().iterator().forEachRemaining(sp -> {
				if (sp instanceof CurrentAction) {
					try {
						sp.execute(daoSauvegarde, game, excecutor, target);
					} catch (UnknowCardException | UnknowHeroException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
			// dégât normaux infligé
			target.addDamage(((Servant) excecutor).getAttack());
			daoSauvegarde.save(target);

			// traitement apres avoir infligé les dégats
			excecutor.setCanAttack(false);
			daoSauvegarde.save(excecutor);

			if (daoServiceEntity.isDead(idTarget)) {// servant mort -> suppression du servant + actionSpecial
				gameResult = deadProcess(game, playerEnemy, target);
			} else {
				gameResult = daoServiceGame.get(game.getIdGame());
			}
		}

		return gameResult;
	}


	@Override
	public Game check(Long idPlayer, Long idGame) throws CantPlayException, UnknowHeroException {
		// verifie si le joueur peut jouer
		Game game = daoServiceGame.get(idGame);
		getPlayerIfCanPlay(game, idPlayer);

		daoServiceGame.turnTimeOut(idGame);// inversement du tour dans la game
		turnManager.initTurn(idGame);
		turnManager.activeTimer(idGame);
		return daoServiceGame.get(idGame);
	}

	@Override
	public Game forfeit(Long idGame, Long idPlayer) throws CantPlayException {
		// verifie si le joueur peut jouer
		Game game = daoServiceGame.get(idGame);
		Player player = getPlayerIfCanPlay(game, idPlayer);

		player.getHero().setHealthPoint(0);
		turnManager.removeTimer(idGame);
		// suppression de tous ce qui concerne cette partie
		daoServiceGame.deleteGame(game);

		return game;
	}

	/**
	 * retourne le joueur qui joue a ce tour
	 * 
	 * @param game
	 * @return
	 */
	private Player findPlayerTurn(Game game) {
		if (game.getPlayerTurn() == (byte) 1) {
			return game.getPlayer1();
		}
		return game.getPlayer2();
	}

	/**
	 * verifie que l'id passer en parametre est bien l'id du joueur courrant
	 * 
	 * @param game
	 * @param idPlayer
	 * @return
	 * @throws CantPlayException
	 */
	private Player getPlayerIfCanPlay(Game game, Long idPlayer) throws CantPlayException {
		// verifie si le joueur peut jouer
		Player player = findPlayerTurn(game);
		if (player.getIdPlayer() != idPlayer)
			throw new CantPlayException();
		return player;
	}

	/**
	 * verifie que le joueur a bien la carte cardName dans sa main
	 * 
	 * @param player
	 * @param cardName
	 * @return
	 */
	private boolean playerHaveCardInHisHand(Player player, String cardName) {
		return player.getHand().contains(cardName);
	}

	/**
	 * verifie si le joueur a asser de mana
	 * 
	 * @return
	 */
	private boolean enoughMana(Player p, int manaMin) {
		return manaMin <= p.getMana();
	}

	/**
	 * retourne l'entite si le joueur peut la jouer (sur son plateau ou hero)
	 * 
	 * @param player
	 * @param idExcecutor
	 * @return
	 */
	private Entity getAndVerifyEntity(Player player, Long idExcecutor) {
		if (player.getHero().getIdEntity().equals(idExcecutor)) {
			return player.getHero();
		} else {
			for (Entity e : player.getPlayingCard()) {
				if (e.getIdEntity().equals(idExcecutor)) {
					return (Servant) e;
				}
			}
		}
		// idExcecutor non trouvé sur le plateau ni comme hero
		throw new IllegalArgumentException("executeur non trouver");
	}

	/**
	 * retourne l'entiten si presente dans la game
	 * 
	 * @param game
	 * @param idTarget
	 * @return Entity
	 */
	private Entity getAndVerifyEntity(Game game, Long idTarget) {
		if (game.getPlayer1().getHero().getIdEntity().equals(idTarget)) {
			return game.getPlayer1().getHero();
		} else if (game.getPlayer2().getHero().getIdEntity().equals(idTarget)) {
			return game.getPlayer2().getHero();
		} else {
			for (Entity e : game.getPlayer1().getPlayingCard()) {
				if (e.getIdEntity().equals(idTarget)) {
					return (Servant) e;
				}
			}
			for (Entity e : game.getPlayer2().getPlayingCard()) {
				if (e.getIdEntity().equals(idTarget)) {
					return (Servant) e;
				}
			}
		}
		// idExcecutor non trouvé sur le plateau
		throw new IllegalArgumentException("target non trouver");
	}

	/**
	 * retourne le joueur adverse du tour en cour
	 * 
	 * @param game
	 * @param player
	 * @return
	 */
	private Player getPlayerEnemy(Game game, Player player) {
		return game.getPlayer1().equals(player) ? game.getPlayer2() : game.getPlayer1();
	}

	@Override
	public TurnManager getTurnManager() {
		return this.turnManager;
	}

	/**
	 * traitement effectuer quand une cible estmort soit fin de la partie dans le
	 * cas du hero mort soit le retrait + activation des action spe pour un servant
	 * 
	 * @param game
	 * @param playerEnemy
	 * @param target
	 * @return
	 */
	private Game deadProcess(Game game, Player playerEnemy, Entity target) {
		if (target instanceof Servant) {
			((Servant) target).getSpecialAction().iterator().forEachRemaining(sp -> {
				if (sp instanceof DeadAction) {
					try {
						sp.execute(daoSauvegarde, game, target, null);
					} catch (UnknowCardException | UnknowHeroException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
			daoServicePlayer.deleteServant(playerEnemy, (Servant) target);
		} else if (target instanceof Hero) {
			Game res = daoServiceGame.get(game.getIdGame());
			daoServiceGame.deleteGame(game);
			return res;
		}
		return daoServiceGame.get(game.getIdGame());
	}

	/**
	 * supprime les entite mortes il y en a
	 * @param game
	 * @param playerEnemy
	 * @return
	 */
	private Game processIfDead(Game game, Player playerEnemy) {
		List<Entity> l = new ArrayList<Entity>();
		playerEnemy.getPlayingCard().iterator().forEachRemaining( e -> {
			if(e.getHealthPoint() <= 0) {
				l.add(e);
				((Servant) e).getSpecialAction().iterator().forEachRemaining(sp -> {
					if (sp instanceof DeadAction) {
						try {
							sp.execute(daoSauvegarde, game, e, null);
						} catch (UnknowCardException | UnknowHeroException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}

					}
				});
				
			}
		});
		for(Entity et: l) {
			daoServicePlayer.deleteServant(playerEnemy, (Servant) et);
		}
		return daoServiceGame.get(game.getIdGame());
	}

	
	/**
	 * verifie si il y a des serviteur avec provocation
	 * 
	 * @param playerEnemy
	 * @return
	 */
	private boolean haveProvocation(Player playerEnemy) {
		for (Entity e : playerEnemy.getPlayingCard()) {
			if ((e instanceof Servant) && ((Servant) e).isProvocation()) {
				return true;
			}
		}
		return false;
	}

}
