package univnantes.hearthstone.controller.Impl;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.stereotype.Controller;

import univnantes.hearthstone.controller.IAccueilController;
import univnantes.hearthstone.controller.IGameController;
import univnantes.hearthstone.controller.IWebSocketController;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.util.clientMessages.*;
import univnantes.hearthstone.util.serverMessages.GameContentMessage;
import univnantes.hearthstone.util.serverMessages.HandMessage;
import univnantes.hearthstone.util.serverMessages.JoinMessage;

@Controller
public class WebSocketController implements IWebSocketController{

	@Autowired
	private IGameController gameController;

	@Autowired
	private IAccueilController accueilController;
	
	@Autowired
	private IDaoServiceGame daoGameService;

    @Override
	public JoinMessage onCreateGame(@DestinationVariable("gameKey") String gameKey,@DestinationVariable("username") String username, @DestinationVariable("userKey") String userKey) throws Exception
    {
        System.out.println("!! WSTEST: message --> "+gameKey+" ; "+username);
        Game createdGame = accueilController.createGame(gameKey, username, userKey);
        
        return new JoinMessage(createdGame.getPlayer1().getIdPlayer(), createdGame.getIdGame());
    }

    @Override
	public HandMessage onGetHand(@DestinationVariable("gameId") Long gameId,@DestinationVariable("gameKey") String gameKey,@DestinationVariable("playerId") Long playerId,@DestinationVariable("playerKey") String playerKey) throws Exception
	{
		secure(gameId,gameKey,playerId,playerKey);
		return new HandMessage(accueilController.getHand(gameId,playerId));
	}

    @Override
    public GameContentMessage onJoinGame(@DestinationVariable("gameId") Long gameId, JoinGameMessage data) throws Exception
    {
    	Game secure = daoGameService.get(gameId);
		if(!data.getGameKey().equals(secure.getkey())) throw new IllegalArgumentException("les données fournies ne permette pas l'accès à ce service");
    	
    	Game game = accueilController.joinGame(gameId, data.getUsername(), data.getUserKey());

        return new GameContentMessage(game,game.getPlayer2().getIdPlayer());
    }

    @Override
    public GameContentMessage onSetHero(@DestinationVariable("gameId") Long gameId, SetHeroMessage data) throws Exception
    {
    	secure(gameId, data.getGameKey(), data.getPlayerId(), data.getPlayerKey());
        System.out.println("!! WSTEST: setheromessage --> "+data.getHero()+" | "+data.getPlayerId()+" | "+data.getGameId());
    	Game game = accueilController.setHero(gameId, data.getPlayerId(), data.getHero());
    	System.out.println("choix du hero. situation de la game : "+game.toString());
        return new GameContentMessage(game,data.getPlayerId());
    }

    @Override
    public GameContentMessage onInvokeCard(@DestinationVariable("gameId") Long gameId, InvokeMessage data) throws Exception
    {
    	secure(gameId, data.getGameKey(), data.getPlayerId(), data.getPlayerKey());
    	 Game game = gameController.invokeCard(data.getPlayerId(), gameId, data.getCardName(), data.getTargetId());
    	 System.out.println("invocation carte "+data.getCardName()+". situation de la game : "+game.toString());
    	 return new GameContentMessage(game,data.getPlayerId());
    }

    @Override
    public GameContentMessage onAction(@DestinationVariable("gameId") Long gameId, ActionMessage data) throws Exception
    {
    	secure(gameId, data.getGameKey(), data.getPlayerId(), data.getPlayerKey());
    	Game game = gameController.action(data.getPlayerId(), gameId, data.getSourceId(), data.getTargetId());
    	System.out.println("action "+data.getSourceId()+" vers la cible "+ data.getTargetId()+" . situation de la game : "+game.toString());

    	return new GameContentMessage(game,data.getPlayerId());
    }

    @Override
    public GameContentMessage onCheck(@DestinationVariable("gameId") Long gameId, CheckMessage data) throws Exception
    {
    	secure(gameId, data.getGameKey(), data.getPlayerId(), data.getPlayerKey());
    	 Game game = gameController.check(data.getPlayerId(), gameId);
     	System.out.println("check de "+data.getPlayerId()+" . situation de la game : "+game.toString());

    	 return new GameContentMessage(game,data.getPlayerId());
    }

	@Override
	public GameContentMessage onForfeit(@DestinationVariable("gameId") Long gameId, ForfeitMessage data) throws Exception {
    	secure(gameId, data.getGameKey(), data.getPlayerId(), data.getPlayerKey());
		Game game = gameController.forfeit(gameId, data.getPlayerId());
		return new GameContentMessage(game,data.getPlayerId());
	}

	/**
	 * verifie que les clés gameKey et userKey corresponde bien à celles connue dans la base de données
	 * @param gameId
	 * @param gameKey
	 * @param playerId
	 * @param playerKey
	 */
	private void secure(Long gameId, String gameKey, Long playerId, String playerKey) {
		Game game = daoGameService.get(gameId);
		if(game == null) throw new IllegalArgumentException("les données fournies ne permette pas l'accès à ce service");
	
		Player player = game.getPlayer1().getIdPlayer().equals(playerId) ? game.getPlayer1() : game.getPlayer2();
		if(player == null) throw new IllegalArgumentException("les données fournies ne permette pas l'accès à ce service");

		if(!gameKey.equals(game.getkey()) || !playerKey.equals(player.getKey())) throw new IllegalArgumentException("les données fournies ne permette pas l'accès à ce service");
	}

}
