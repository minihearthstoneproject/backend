package univnantes.hearthstone.controller.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import univnantes.hearthstone.controller.IAccueilController;
import univnantes.hearthstone.controller.IGameController;
import univnantes.hearthstone.dao.services.IDaoServiceGame;
import univnantes.hearthstone.dao.services.IDaoServicePlayer;
import univnantes.hearthstone.exception.AlreadyHaveHeroException;
import univnantes.hearthstone.exception.GameAlreadyFullException;
import univnantes.hearthstone.exception.UnknowHeroException;
import univnantes.hearthstone.modele.Game;
import univnantes.hearthstone.modele.Hero;
import univnantes.hearthstone.modele.Player;
import univnantes.hearthstone.modele.factories.HeroFactory;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccueilControllerImpl implements IAccueilController {

	@Autowired
	private IDaoServiceGame daoServiceGame;

	@Autowired
	private IDaoServicePlayer daoServicePlayer;

	@Autowired
	private IGameController gameController;

	@Autowired
	private HeroFactory heroFactory;

	@Override
	public Game createGame(String gameKey, String userName, String key) {
		Player player1 = daoServicePlayer.createPlayer(userName, key);
		return daoServiceGame.createGameWithPlayer(gameKey, player1);
	}

	@Override
	public Game joinGame(Long idGame, String userName, String key) throws GameAlreadyFullException {
		// deja deux joueurs
		if (daoServiceGame.isFull(idGame))
			throw new GameAlreadyFullException();

		Player player2 = daoServicePlayer.createPlayer(userName, key);
		return daoServiceGame.joinGameWithId(idGame, player2);
	}

	@Override
	public Game setHero(Long idGame, Long idUser, String heroName)
			throws AlreadyHaveHeroException, UnknowHeroException {

		// deja un hero pour ce joueur
		if (daoServicePlayer.alreadyHaveHero(idUser))
			throw new AlreadyHaveHeroException(idUser);

		Hero hero = heroFactory.createHero(heroName);
		daoServicePlayer.setHero(idUser, hero);

		Game game = daoServiceGame.get(idGame);
		if (game.getPlayer1() != null && game.getPlayer1().getHero() != null && game.getPlayer2() != null
				&& game.getPlayer2().getHero() != null) {
			gameController.getTurnManager().initGame(game);
			gameController.getTurnManager().activeTimer(idGame);// activation du chrono
		}

		return daoServiceGame.get(idGame);

	}

	@Override
	public List<String> getHand(Long idGame, Long idUser)
	{
		Game game = daoServiceGame.get(idGame);
		if(idUser == game.getPlayer1().getIdPlayer())
		{
			return game.getPlayer1().getHand();
		}
		else if(idUser == game.getPlayer2().getIdPlayer())
		{
			return game.getPlayer2().getHand();
		}
		else{
			return new ArrayList<String>();
		}
	}

}
