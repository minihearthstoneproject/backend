package univnantes.hearthstone.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;

import univnantes.hearthstone.util.clientMessages.*;
import univnantes.hearthstone.util.serverMessages.GameContentMessage;
import univnantes.hearthstone.util.serverMessages.HandMessage;
import univnantes.hearthstone.util.serverMessages.JoinMessage;

public interface IWebSocketController {
    @SubscribeMapping("/createGame/{gameKey}/{username}/{userKey}")
    JoinMessage onCreateGame(@DestinationVariable("gameKey") String gameKey,@DestinationVariable("username") String username,@DestinationVariable("userKey") String userKey) throws Exception;

    @SubscribeMapping("/getHand/{gameId}/{gameKey}/{playerId}/{playerKey}")
    HandMessage onGetHand(@DestinationVariable("gameId") Long gameId,@DestinationVariable("gameKey") String gameKey,@DestinationVariable("playerId") Long playerId,@DestinationVariable("playerKey") String playerKey) throws Exception;

    @MessageMapping("/joinGame/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onJoinGame(@DestinationVariable("gameId") Long gameId,@Payload JoinGameMessage data) throws Exception;

    @MessageMapping("/setHero/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onSetHero(@DestinationVariable("gameId") Long gameId, @Payload SetHeroMessage data) throws Exception;

    @MessageMapping("/invokeCard/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onInvokeCard(@DestinationVariable("gameId") Long gameId,@Payload InvokeMessage data) throws Exception;

    @MessageMapping("/action/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onAction(@DestinationVariable("gameId") Long gameId, @Payload ActionMessage data) throws Exception;

    @MessageMapping("/check/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onCheck(@DestinationVariable("gameId") Long gameId, @Payload CheckMessage data) throws Exception;

    @MessageMapping("/forfeit/{gameId}")
    @SendTo("/game/{gameId}")
    GameContentMessage onForfeit(@DestinationVariable("gameId") Long gameId, @Payload ForfeitMessage data) throws Exception;
}
