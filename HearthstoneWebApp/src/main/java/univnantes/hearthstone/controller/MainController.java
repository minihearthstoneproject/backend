package univnantes.hearthstone.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Controller
@RequestMapping("/accueil")
public class MainController {


	@GetMapping
	@ResponseBody
	Object accueil() {
		return "accueil";
	}
 
	
	@GetMapping("/staticTests")
	Object getResultOfStaticTests(Model model) {
		return "dao/pmd";
	}
}