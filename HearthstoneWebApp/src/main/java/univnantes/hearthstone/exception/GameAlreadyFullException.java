package univnantes.hearthstone.exception;

import java.util.logging.Logger;

public class GameAlreadyFullException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4547080830103445770L;
	public static Logger logger = Logger.getLogger("GameAlreadyFullException");
	
	
	public GameAlreadyFullException() {
		logger.warning("partie deja complete");
	}
}
