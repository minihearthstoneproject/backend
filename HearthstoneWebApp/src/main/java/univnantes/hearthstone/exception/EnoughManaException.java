package univnantes.hearthstone.exception;

import java.util.logging.Logger;

public class EnoughManaException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4741442381810626302L;
	public static Logger logger = Logger.getLogger("EnoughManaException");

	public EnoughManaException() {
		logger.warning("pas assez de mmana");
	}
}
