package univnantes.hearthstone.exception;

import java.util.logging.Logger;

public class CantPlayException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Logger logger = Logger.getLogger("CantPlayException");
	public CantPlayException() {
	logger.warning("is not your turn or entity cant attack");	
	}
}
