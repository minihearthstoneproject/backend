# Projet d'analyse conception de logiciel

## Tiny Hearthstone

## Auteurs
* Maxence COUTAND 
* Nicolas VANNIER
* Tom LE BERRE


## Compilation et execution
1) depuis un terminal se rendre dans le dossier de HearthstoneWebApp

-> mvn package -f pom.xml

2) se rendre dans le dossier HearthstoneWebApp/target et localiser l'archive HearthstoneWebApp-0.0.1-SNAPSHOT.war

-> jar -xvf HearthstoneWebApp-0.0.1-SNAPSHOT.war

-> cd WEB-INF

-> java -classpath "lib/*:classes/." univnantes.hearthstone.HearthstoneWebAppApplication